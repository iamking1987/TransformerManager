﻿using System;

namespace TransformerManager.Shared.Dto.Tank
{
    public class TankDto
    {
        public int Id { get; set; }

        public Guid TransformerId { set; get; }

        public double TankLength { set; get; }

        public double TankWidth { set; get; }

        public double TankHeight { set; get; }

        public string TankShape { set; get; }

        public string FrameType { set; get; }

        // 蝶阀类型
        public string RadButterflyValveType { set; get; }

        // 开关挖槽
        public string OltcIndent { set; get; }

        // 高低压挖槽
        public string HVLVIndent { set; get; }

        // 空气母线箱接口
        public string ActbDome { set; get; }

        // 油油母线箱接口
        public string OctbDome { set; get; }

        // 是否有小车
        public bool HasWheel { set; get; }

        // 是否有中性点升高座
        public bool HasNeutralDome { set; get; }

        // 侧壁1是否有鼓包
        public bool HasEnlargeSideWall1 { set; get; }

        // 侧壁2是否有鼓包
        public bool HasEnlargeSideWall2 { set; get; }

        // 侧壁3是否有鼓包
        public bool HasEnlargeSideWall3 { set; get; }

        // 侧壁4是否有鼓包
        public bool HasEnlargeSideWall4 { set; get; }
    }
}
