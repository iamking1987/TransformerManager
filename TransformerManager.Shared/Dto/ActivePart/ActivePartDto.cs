﻿using System;

namespace TransformerManager.Shared.Dto.ActivePart
{
    public class ActivePartDto
    {
        public int Id { get; set; }

        public Guid TransformerId { set; get; }

        // 撑条数
        public int StripNumber { set; get; }

        // 垫块数
        public int SpacerNumber { set; get; }

        // 电压等级
        public int RatedVotage { set; get; }

        // 是否中部进线
        public bool IsCenterInfeed { set; get; }

        // 是否全绝缘
        public bool IsUniformInsulation { set; get; }

        // 线圈个数
        public int WindingNumber { set; get; }

        // 调压类型
        public string HVRType { set; get; }

        // 最内线圈形式
        public string InnestWindingType { set; get; }

        // 高压线圈AC
        public int HVAC { set; get; }

        // 是否有强油导向
        public bool HasOD { set; get; }
    }
}
