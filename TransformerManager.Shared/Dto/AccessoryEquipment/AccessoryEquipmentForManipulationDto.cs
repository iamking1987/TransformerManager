﻿namespace TransformerManager.Shared.Dto.AccessoryEquipment
{
    public abstract class AccessoryEquipmentForManipulationDto
    {
        // 附件装备名称
        public string AccessoryEquName { set; get; }

        // 产品型号
        public string ProdType { set; get; }

        // 制造商
        public string Manufacturer { set; get; }
    }
}
