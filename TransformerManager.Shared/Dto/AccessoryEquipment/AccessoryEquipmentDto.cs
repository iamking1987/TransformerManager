﻿using System;

namespace TransformerManager.Shared.Dto.AccessoryEquipment
{
    public class AccessoryEquipmentDto
    {
        public int Id { get; set; }

        public int AccessoriesId { get; set; }


        // 附件装备名称
        public string AccessoryEquName { set; get; }

        // 产品型号
        public string ProdType { set; get; }

        // 制造商
        public string Manufacturer { set; get; }
    }
}
