﻿namespace TransformerManager.Shared.Dto.TapChanger
{
    public abstract class TapChangerForManipulationDto
    {
        public string ProdType { set; get; }

        public string TapChangerPosition { set; get; }

        public string OperationType { set; get; }

        public string Manufacturer { set; get; }
    }
}
