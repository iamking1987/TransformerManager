﻿using System;

namespace TransformerManager.Shared.Dto.TapChanger
{
    public class TapChangerDto
    {
        public int Id { get; set; }

        public int BasicInfosId { set; get; }

        public string ProdType { set; get; }

        public string TapChangerPosition { set; get; }

        public string OperationType { set; get; }

        public string Manufacturer { set; get; }
    }
}
