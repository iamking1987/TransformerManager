﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using TransformerManager.Shared.Dto.Bushing;
using TransformerManager.Shared.Dto.Dome;
using TransformerManager.Shared.Dto.TapChanger;

namespace TransformerManager.Shared.Dto.BasicInfos
{
    public class BasicInfosForUpdateDto : BasicInfosForManipulationDto
    {
    }
}
