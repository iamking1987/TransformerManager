﻿using System.ComponentModel.DataAnnotations;
using TransformerManager.Shared.Dto.Accessories;
using TransformerManager.Shared.Dto.ActivePart;
using TransformerManager.Shared.Dto.BasicInfos;
using TransformerManager.Shared.Dto.Conservator;
using TransformerManager.Shared.Dto.CoolEquComp;
using TransformerManager.Shared.Dto.Lead;
using TransformerManager.Shared.Dto.Tank;
using TransformerManager.Shared.Dto.Winding;

namespace TransformerManager.Shared.Dto.Transformer
{
    public class TransformerForUpdateDto : TransformerForManipulationDto
    {
        
    }
}
