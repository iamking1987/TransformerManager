﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TransformerManager.Shared.Dto.Accessories;
using TransformerManager.Shared.Dto.ActivePart;
using TransformerManager.Shared.Dto.BasicInfos;
using TransformerManager.Shared.Dto.Bushing;
using TransformerManager.Shared.Dto.Conservator;
using TransformerManager.Shared.Dto.CoolEquComp;
using TransformerManager.Shared.Dto.Dome;
using TransformerManager.Shared.Dto.Lead;
using TransformerManager.Shared.Dto.Tank;
using TransformerManager.Shared.Dto.TapChanger;

namespace TransformerManager.Shared.Dto.Transformer
{
    public class TransformerDto
    {
        public TransformerDto()
        {
            BasicInfos = new BasicInfosDto();
            Tank = new TankDto();
            Conservator = new ConservatorDto();
            CoolingSystem = new CoolingSystemDto();
            Accessories = new AccessoriesDto();
            Lead = new LeadDto();
            Winding = new WindingDto();
            ActivePart = new ActivePartDto();
        }

        public Guid Id { set; get; }

        public string UserId { set; get; }

        public BasicInfosDto BasicInfos { set; get; }

        public TankDto Tank { set; get; }

        public ConservatorDto Conservator { set; get; }

        public CoolingSystemDto CoolingSystem { set; get; }

        public AccessoriesDto Accessories { set; get; }

        public LeadDto Lead { set; get; }

        public WindingDto Winding { set; get; }

        public ActivePartDto ActivePart { set; get; }

        public DateTime CreateTime { get; set; }

        public DateTime? UpdateTime { get; set; }

    }
}
