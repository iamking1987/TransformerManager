﻿using System;
using System.ComponentModel.DataAnnotations;
using TransformerManager.Shared.Dto.Accessories;
using TransformerManager.Shared.Dto.ActivePart;
using TransformerManager.Shared.Dto.BasicInfos;
using TransformerManager.Shared.Dto.Conservator;
using TransformerManager.Shared.Dto.CoolEquComp;
using TransformerManager.Shared.Dto.Lead;
using TransformerManager.Shared.Dto.Tank;
using TransformerManager.Shared.Dto.Winding;

namespace TransformerManager.Shared.Dto.Transformer
{
    public abstract class TransformerForManipulationDto
    {
        public string UserId { set; get; }

        public BasicInfosForUpdateDto BasicInfos { set; get; }

        public TankForUpdateDto Tank { set; get; }

        public ConservatorForUpdateDto Conservator { set; get; }

        public CoolingSystemForUpdateDto CoolingSystem { set; get; }

        public AccessoriesForUpdateDto Accessories { set; get; }

        public LeadForUpdateDto Lead { set; get; }

        public WindingForUpdateDto Winding { set; get; }

        public ActivePartForUpdateDto ActivePart { set; get; }

        public DateTime CreateTime { get; set; }

        public DateTime? UpdateTime { get; set; }
    }
}
