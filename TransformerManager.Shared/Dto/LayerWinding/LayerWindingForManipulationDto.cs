﻿namespace TransformerManager.Shared.Dto.LayerWinding
{
    public abstract class LayerWindingForManipulationDto
    {
        // 线圈名称
        public string WindingName { set; get; }

        // 层式线圈类型
        public string LayerWindingType { set; get; }

        // 内部绕线方向
        public string InnerLayerDirection { set; get; }

        // 撑条数
        public int StripNumber { set; get; }

        // 垫块数
        public int SpacerNumber { set; get; }

        // 垫块宽度
        public double SpacerWidth { set; get; }

        // 轴向并绕根数
        public int AsialConductorNumber { set; get; }

        // 辐向并绕根数
        public int RadialConductorNumber { set; get; }

        // 是否有静电环
        public bool HasFSA { set; get; }

        // 挡油圈数量
        public int OilDeflectorNumber { set; get; }

        // 是否是全绝缘
        public bool IsUniformInsulation { set; get; }

        // 内径侧纸筒数量
        public int InnerCylinderNumber { set; get; }

        // 是否有铁芯屏蔽
        public bool HasCoreShielding { set; get; }

        // 是否端部拉平
        public bool IsLever { set; get; }

        // 是否分为上下两部分
        public bool IsDivided { set; get; }
    }
}
