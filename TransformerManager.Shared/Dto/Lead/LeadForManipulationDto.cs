﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using TransformerManager.Shared.Dto.LeadConnector;

namespace TransformerManager.Shared.Dto.Lead
{
    public abstract class LeadForManipulationDto
    {
        // 高压电流
        public double HVCurrent { set; get; }

        // 低压电流
        public double LVCurrent { set; get; }

        // 中压电流
        public double MVCurrent { set; get; }

        // 是否中部出线
        public bool IsCenterInfeed { set; get; }

        // 高压出头方式
        public string HVOutlet { set; get; }

        // 调压出头方式
        public string HVROutlet { set; get; }

        // 低压出头方式
        public string LVOutlet { set; get; }

        // 中压出头方式
        public string MVOutlet { set; get; }

        // 高压套管位置
        public string HVBushingPos { set; get; }

        // 低压套管位置
        public string LVBushingPos { set; get; }

        // 中压套管位置
        public string MVBushingPos { set; get; }

        // 引线连接件
        public ICollection<LeadConnectorForUpdateDto> LeadConnectors { set; get; }
            = new ObservableCollection<LeadConnectorForUpdateDto>();
    }
}
