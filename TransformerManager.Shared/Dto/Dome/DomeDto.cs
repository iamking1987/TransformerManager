﻿using System;

namespace TransformerManager.Shared.Dto.Dome
{
    public class DomeDto
    {
        public int Id { get; set; }

        public int BasicInfosId { set; get; }

        public string DomeAssembleType { set; get; }

        public double InnerDiameter { set; get; }

    }
}
