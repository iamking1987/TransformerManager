﻿namespace TransformerManager.Shared.Dto.Dome
{
    public abstract class DomeForManipulationDto
    {
        public string DomeAssembleType { set; get; }

        public double InnerDiameter { set; get; }
    }
}
