﻿using System;

namespace TransformerManager.Shared.Dto.Conservator
{
    public class ConservatorDto
    {
        public int Id { get; set; }

        public Guid TransformerId { set; get; }

        public string ConservatorType { set; get; }

        public bool HasTCConservator { set; get; }

        public string Dimension { set; get; }

    }
}
