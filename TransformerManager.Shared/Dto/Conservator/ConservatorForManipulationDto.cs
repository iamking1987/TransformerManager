﻿namespace TransformerManager.Shared.Dto.Conservator
{
    public abstract class ConservatorForManipulationDto
    {
        public string ConservatorType { set; get; }

        public bool HasTCConservator { set; get; }

        public string Dimension { set; get; }
    }
}
