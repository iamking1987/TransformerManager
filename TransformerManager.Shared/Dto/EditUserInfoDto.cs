﻿using System.ComponentModel.DataAnnotations;

namespace TransformerManager.Shared.Dto
{
    public class EditUserInfoDto
    {
        [Required(ErrorMessage = "账户不可为空")]
        public string UserName { set; get; }

        public string RealNameEn { set; get; }

        public string RealNameCn { set; get; }

        public string Department { set; get; }

        public bool IsLeadEngineer { set; get; }
    }
}
