﻿namespace TransformerManager.Shared.Dto.Bushing
{
    public abstract class BushingForManipulationDto
    {
        public string InsulatorColor { set; get; }

        // CT可安装总高
        public double CTTotalAssembleHeight { set; get; }

        // 型号
        public string ProdType { set; get; }

        // 参考标准
        public string RefStandard { set; get; }

        // 制造商
        public string Manufacturer { set; get; }
    }
}
