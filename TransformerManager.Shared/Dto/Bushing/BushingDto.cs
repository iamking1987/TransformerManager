﻿using System;

namespace TransformerManager.Shared.Dto.Bushing
{
    public class BushingDto
    {
        public int Id { get; set; }

        public int BasicInfosId { set; get; }

        // 套管颜色
        public string InsulatorColor { set; get; }

        // CT可安装总高
        public double? CTTotalAssembleHeight { set; get; }

        // 型号
        public string ProdType { set; get; }

        // 参考标准
        public string RefStandard { set; get; }

        // 制造商
        public string Manufacturer { set; get; }
    }
}
