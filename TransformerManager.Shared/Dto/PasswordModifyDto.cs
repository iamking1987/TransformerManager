﻿using System.ComponentModel.DataAnnotations;

namespace TransformerManager.Shared.Dto
{
    public class PasswordModifyDto
    {
        [Required(ErrorMessage = "用户名不可为空")]
        public string UserName { set; get; }

        [Required(ErrorMessage = "当前密码不可为空")]
        public string CurrentPassword { set; get; }

        [Required(ErrorMessage = "新密码不可为空")]
        [MinLength(6)]
        public string NewPassword { set; get; }

        [Required(ErrorMessage = "新密码不可为空")]
        [Compare(nameof(NewPassword), ErrorMessage = "两次密码不一致")]
        [MinLength(6)]
        public string ConfirmedNewPassword { set; get; }
    }
}
