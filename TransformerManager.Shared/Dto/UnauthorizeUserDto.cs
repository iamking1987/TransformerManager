﻿namespace TransformerManager.Shared.Dto
{
    public class UnauthorizeUserDto
    {
        public string UserName { set; get; }

        public string RoleName { set; get; }
    }
}
