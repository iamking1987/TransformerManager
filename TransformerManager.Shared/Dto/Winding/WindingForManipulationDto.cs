﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using TransformerManager.Shared.Dto.DiscWinding;
using TransformerManager.Shared.Dto.HVRWinding;
using TransformerManager.Shared.Dto.LayerWinding;

namespace TransformerManager.Shared.Dto.Winding
{
    public abstract class WindingForManipulationDto
    {
        // 调压线圈
        public ICollection<HVRWindingForUpdateDto> HVRWindings { set; get; }
            = new ObservableCollection<HVRWindingForUpdateDto>();

        // 饼式线圈
        public ICollection<DiscWindingForUpdateDto> DiscWindings { set; get; }
            = new ObservableCollection<DiscWindingForUpdateDto>();

        // 层式线圈
        public ICollection<LayerWindingForUpdateDto> LayerWindings { set; get; }
            = new ObservableCollection<LayerWindingForUpdateDto>();
    }
}
