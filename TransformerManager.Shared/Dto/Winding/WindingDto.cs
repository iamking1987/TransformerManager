﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using TransformerManager.Shared.Dto.DiscWinding;
using TransformerManager.Shared.Dto.HVRWinding;
using TransformerManager.Shared.Dto.LayerWinding;

namespace TransformerManager.Shared.Dto
{
    public class WindingDto
    {
        public int Id { get; set; }

        public Guid TransformerId { set; get; }

        // 调压线圈
        public ObservableCollection<HVRWindingDto> HVRWindings { set; get; }
            = new ObservableCollection<HVRWindingDto>();

        // 饼式线圈
        public ObservableCollection<DiscWindingDto> DiscWindings { set; get; }
            = new ObservableCollection<DiscWindingDto>();

        // 层式线圈
        public ObservableCollection<LayerWindingDto> LayerWindings { set; get; }
            = new ObservableCollection<LayerWindingDto>();
    }
}
