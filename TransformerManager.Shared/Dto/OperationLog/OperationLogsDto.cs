﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TransformerManager.Shared.Dto.OperationLog
{
    public class OperationLogsDto
    {
        public int Id { get; set; }

        public string Level { get; set; }

        public DateTime TimeStamp { get; set; }

        public string Exception { get; set; }

        public string OperationType { get; set; }

        public string UserName { get; set; }

        public string IP { set; get; }

        public string RequestPath { set; get; }
    }
}
