﻿using System;

namespace TransformerManager.Shared.Dto.HVRWinding
{
    public class HVRWindingDto
    {
        public int Id { get; set; }

        public int WindingId { set; get; }

        // 调压线圈类型
        public string HVRWindingType { set; get; }

        // 绕向
        public string WindingDirection { set; get; }

        // 撑条数
        public int StripNumber { set; get; }

        // 垫块数
        public int SpacerNumber { set; get; }

        // 垫块宽度
        public double SpacerWidth { set; get; }

        // 并绕根数
        public int ConductorNumber { set; get; }

        // 是否有静电环
        public bool HasFSA { set; get; }

        // 是否是全绝缘
        public bool IsUniformInsulation { set; get; }

        // 是否是CTC导线
        public bool IsCTC { set; get; }

        // 调压级数
        public int HVRStepNumber { set; get; }

        // 调压上部小角环数
        public int TopKsNumber { set; get; }

        // 调压线圈大角环数
        public int AngleRingNumber { set; get; }

        // 是否分两部分
        public bool IsDivided { set; get; }
    }
}
