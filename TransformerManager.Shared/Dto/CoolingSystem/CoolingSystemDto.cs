﻿using System;

namespace TransformerManager.Shared.Dto.CoolEquComp
{
    public class CoolingSystemDto
    {
        public int Id { get; set; }

        public Guid TransformerId { set; get; }

        // 冷却方式
        public string CoolingType { set; get; }

        // 布置方式
        public string CoolingLayout { set; get; }

        // 片散型号
        public string RadiatorType { set; get; }

        // 片散制造商
        public string RadiatorManufacturer { set; get; }

        // 风机型号
        public string FanType { set; get; }

        // 风机制造商
        public string FanManufacturer { set; get; }

        // 冷却器型号
        public string CoolerType { set; get; }

        // 冷却器制造商
        public string CoolerManufacturer { set; get; }
    }
}
