﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using TransformerManager.Shared.Dto.AccessoryEquipment;

namespace TransformerManager.Shared.Dto.Accessories
{
    public abstract class AccessoriesForManipulationDto
    {
        public string BoltMaterial { set; get; }

        public string SealMaterial { set; get; }

        // 附件装备
        public ICollection<AccessoryEquipmentForUpdateDto> AccesstoryEquipments { set; get; }
            = new ObservableCollection<AccessoryEquipmentForUpdateDto>();

    }
}
