﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using TransformerManager.Shared.Dto.AccessoryEquipment;

namespace TransformerManager.Shared.Dto.Accessories
{
    public class AccessoriesDto
    {
        public int Id { get; set; }

        public Guid TransformerId { set; get; }

        // 螺栓材质
        public string BoltMaterial { set; get; }

        // 螺栓材质
        public string SealMaterial { set; get; }

        // 附件装备
        public ObservableCollection<AccessoryEquipmentDto> AccesstoryEquipments { set; get; }
            = new ObservableCollection<AccessoryEquipmentDto>();
    }
}
