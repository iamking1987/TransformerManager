﻿using System.ComponentModel.DataAnnotations;

namespace TransformerManager.Shared.Dto
{
    public class RegisterDto
    {
        [Required(ErrorMessage = "账户名不可为空")]
        public string UserName { set; get; }

        [Required(ErrorMessage = "用户拼音名称不可为空")]
        public string RealNameEn { set; get; }

        [Required(ErrorMessage = "中文名称不可为空")]
        public string RealNameCn { set; get; }

        [Required(ErrorMessage = "部门信息不可为空")]
        public string Department { set; get; }

        public bool IsLeadEngineer { set; get; }


        [Required(ErrorMessage = "密码不可为空")]
        public string Password { set; get; }

        [Required]
        [Compare(nameof(Password), ErrorMessage = "两次密码不一致")]
        public string ConfirmPassword { set; get; }
    }
}
