﻿using System.ComponentModel.DataAnnotations;

namespace TransformerManager.Shared.Dto
{
    public class LoginDto
    {
        [Required(ErrorMessage = "账户不可为空")]
        public string UserName { set; get; }

        [Required(ErrorMessage = "密码不可为空")]
        public string Password { set; get; }
    }
}
