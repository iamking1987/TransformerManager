﻿

namespace TransformerManager.Shared.Dto.LeadConnector
{
    public abstract class LeadConnectorForManipulationDto
    {
        // 引线名称
        public string LeadName { set; get; }

        // 引线类别
        public string LeadType { set; get; }

        // 引线尺寸类别
        public string LeadDimensionType { set; get; }

        // 绝缘厚度
        public double Si { set; get; }
    }
}
