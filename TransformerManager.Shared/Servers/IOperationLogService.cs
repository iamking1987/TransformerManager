﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TransformerManager.Shared.Helper;
using TransformerManager.EF.Models;
using TransformerManager.Shared.ResourceParameters;

namespace TransformerManager.Shared.Servers
{
    public interface IOperationLogService
    {
        Task<PaginationList<OperationLogs>> GetOperationLogsAsync(
            string userName,
            DateTime operateTimeStart,
            DateTime operateTimeEnd,
            PaginationResourceParameters paginationParams);

        Task<OperationLogs> GetOperationLogByIdAsync(int id);

        Task<int> GetOperationLogsTotalNumAsync();

        void DeleteOperationLogsAsync(int delNum);

        Task<bool> SaveChangesAsync();
    }
}
