﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TransformerManager.Shared.Helper;
using TransformerManager.EF.Models;
using TransformerManager.Shared.ResourceParameters;

namespace TransformerManager.Shared.Servers
{
    public interface ITransformerRepository
    {

        Task<Transformer> GetTransformerAsync(Guid transformerId);
        Task<BasicInfos> GetBasicInfosAsync(Guid transformerId);
        Task<Tank> GetTankAsync(Guid transformerId);
        Task<Conservator> GetConservatorAsync(Guid transformerId);
        Task<CoolingSystem> GetCoolingSystemAsync(Guid transformerId);
        Task<Accessories> GetAccessoriesAsync(Guid transformerId);
        Task<Lead> GetLeadAsync(Guid transformerId);
        Task<Winding> GetWindingAsync(Guid transformerId);
        Task<ActivePart> GetActivePartAsync(Guid transformerId);

        Task<IEnumerable<Transformer>> GetTransformerByIdsAsync(IEnumerable<Guid> ids);

        Task<PaginationList<Transformer>> GetTransformersAsync(
            TransformerResourceParameters transformerResourceParameters,
            PaginationResourceParameters paginationParams);

        Task<PaginationList<Transformer>> GetTransformersAsync(
            PaginationResourceParameters paginationParams);
        void AddTransformer(Transformer transformer);

        // 检测该项目号的变压器是否存在
        Task<bool> CheckTransformerExistAsync(Guid transformerId);
        Task<bool> CheckTransformerExistByProjectNumberAsync(string projectNumber);

        void DeleteTransformer(Transformer transformer);

        void DeleteTransformers(IEnumerable<Transformer> transformers);

        Task<int> GetTransformersNumAsync(string regionType, DateTime from, DateTime end);

        Task<bool> SaveChangesAsync();
    }
}
