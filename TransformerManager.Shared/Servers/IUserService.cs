﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TransformerManager.Shared.Helper;
using TransformerManager.EF.Models;
using TransformerManager.Shared.ResourceParameters;

namespace TransformerManager.Shared.Servers
{
    public interface IUserService
    {
        Task AddRefreshTokenAsync(RefreshToken refreshToken);
        //Task<TokenResult> RefreshTokenAsync(string token, string refreshToken);

        Task<PaginationList<ApplicationUser>> GetUsersInfoAsync(string keywords, bool isLeadEngineer, PaginationResourceParameters paginationParams);

        Task<ApplicationUser> GetUserByIdAsync(string id);

        Task<bool> SaveChangesAsync();
    }
}
