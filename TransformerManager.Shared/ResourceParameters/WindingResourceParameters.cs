﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TransformerManager.Shared.ResourceParameters
{
    public class WindingResourceParameters
    {
        public HVRWindingResourceParameters HVRWinding { set; get; }

        public DiscWindingResourceParameters DiscWinding { set; get; }

        public LayerWindingResourceParameters LayerWinding { set; get; }
    }
}
