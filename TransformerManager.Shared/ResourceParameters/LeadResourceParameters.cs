﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TransformerManager.Shared.ResourceParameters
{
    public class LeadResourceParameters
    {
        // 高压电流
        public double HVCurrent { set; get; } = -1;

        // 低压电流
        public double LVCurrent { set; get; } = -1;

        // 中压电流
        public double MVCurrent { set; get; } = -1;

        // 是否中部出线
        public bool IsCenterInfeed { set; get; } = false;

        // 高压出头方式
        public string HVOutlet { set; get; } = string.Empty;

        // 调压出头方式
        public string HVROutlet { set; get; } = string.Empty;

        // 低压出头方式
        public string LVOutlet { set; get; } = string.Empty;

        // 中压出头方式
        public string MVOutlet { set; get; } = string.Empty;

        // 高压套管位置
        public string HVBushingPos { set; get; } = string.Empty;

        // 低压套管位置
        public string LVBushingPos { set; get; } = string.Empty;

        // 中压套管位置
        public string MVBushingPos { set; get; } = string.Empty;

    }
}
