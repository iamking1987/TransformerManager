﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TransformerManager.Shared.ResourceParameters
{
    public class BushingResourceParameters
    {
        // 套管颜色
        public string InsulatorColor { set; get; } = string.Empty;

        // CT可安装总高
        public double CTTotalAssembleHeight { set; get; } = -1;

        // 型号
        public string ProdType { set; get; } = string.Empty;

        // 参考标准
        public string RefStandard { set; get; } = string.Empty;

        // 制造商
        public string Manufacturer { set; get; } = string.Empty;
    }
}
