﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TransformerManager.Shared.ResourceParameters
{
    public class HVRWindingResourceParameters
    {
        // 调压线圈类型
        public string HVRWindingType { set; get; } = string.Empty;

        // 绕向
        public string WindingDirection { set; get; } = string.Empty;

        // 撑条数
        public int StripNumber { set; get; } = -1;

        // 垫块数
        public int SpacerNumber { set; get; } = -1;

        // 垫块宽度
        public double SpacerWidth { set; get; } = -1;

        // 并绕根数
        public int ConductorNumber { set; get; } = -1;

        // 是否有静电环
        public bool HasFSA { set; get; } = false;

        // 是否是全绝缘
        public bool IsUniformInsulation { set; get; } = false;

        // 是否是CTC导线
        public bool IsCTC { set; get; } = false;

        // 调压级数
        public int HVRStepNumber { set; get; } = -1;

        // 调压上部小角环数
        public int TopKsNumber { set; get; } = -1;

        // 调压线圈大角环数
        public int AngleRingNumber { set; get; } = -1;

        // 是否分两部分
        public bool IsDivided { set; get; } = false;
    }
}
