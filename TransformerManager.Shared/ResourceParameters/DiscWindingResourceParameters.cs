﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TransformerManager.Shared.ResourceParameters
{
    public class DiscWindingResourceParameters
    {
        // 线圈名称
        public string WindingName { set; get; } = string.Empty;

        // 饼式线圈类别
        public string DiscWindingType { set; get; } = string.Empty;

        // 绕向
        public string WindingDirection { set; get; } = string.Empty;

        // 撑条数
        public int StripNumber { set; get; } = -1;

        // 垫块数
        public int SpacerNumber { set; get; } = -1;

        // 垫块宽度
        public double SpacerWidth { set; get; } = -1;

        // 并绕根数
        public int ConductorNumber { set; get; } = -1;

        // 是否有静电环
        public bool HasFSA { set; get; } = false;

        // 并绕根数
        public int OilDeflectorNumber { set; get; } = -1;

        // 是否是全绝缘
        public bool IsUniformInsulation { set; get; } = false;

        // 是否是CTC导线
        public bool IsCTC { set; get; } = false;

        // 是否有羊角垫块
        public bool HasBananaSpacer { set; get; } = false;

        // 是否有第二饼出线
        public bool HasSecondDiscOut { set; get; } = false;

        // 是否是插花纠
        public bool IsSandwichInterleaved { set; get; } = false;

        // 是否是普纠
        public bool IsInterleaved { set; get; } = false;

        // 导线芯数
        public int ConductorStrandNumber { set; get; } = -1;

        // 是否分为两部分
        public bool IsDivided { set; get; } = false;

        // 是否有引线板
        public bool HasLeadingPlate { set; get; } = false;

        // 是否有硬纸筒
        public bool HasFormCylinder { set; get; } = false;
    }
}
