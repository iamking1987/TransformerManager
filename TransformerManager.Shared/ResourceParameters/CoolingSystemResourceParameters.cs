﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TransformerManager.Shared.ResourceParameters
{
    public class CoolingSystemResourceParameters
    {
        // 冷却方式
        public string CoolingType { set; get; } = string.Empty;

        // 布置方式
        public string CoolingLayout { set; get; } = string.Empty;

        // 片散型号
        public string RadiatorType { set; get; } = string.Empty;

        // 片散制造商
        public string RadiatorManufacturer { set; get; } = string.Empty;

        // 风机型号
        public string FanType { set; get; } = string.Empty;

        // 风机制造商
        public string FanManufacturer { set; get; } = string.Empty;

        // 冷却器型号
        public string CoolerType { set; get; } = string.Empty;

        // 冷却器制造商
        public string CoolerManufacturer { set; get; } = string.Empty;
    }
}
