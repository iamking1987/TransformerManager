﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TransformerManager.Shared.ResourceParameters
{
    public class ConservatorResourceParameters
    {
        // 储油柜类型
        public string ConservatorType { set; get; } = string.Empty;

        // 是否有附储油柜
        public bool HasTCConservator { set; get; } = false;

        // 尺寸
        public string Dimension { set; get; } = string.Empty;
    }
}
