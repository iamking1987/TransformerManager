﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TransformerManager.Shared.ResourceParameters
{
    public class LeadConnectorResourceParameters
    {
        // 引线名称
        public string LeadName { set; get; } = string.Empty;

        // 引线类别
        public string LeadType { set; get; } = string.Empty;

        // 引线尺寸类别
        public string LeadDimensionType { set; get; } = string.Empty;

        // 绝缘厚度
        public double Si { set; get; } = -1;
    }
}
