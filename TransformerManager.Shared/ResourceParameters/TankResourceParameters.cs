﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TransformerManager.Shared.ResourceParameters
{
    public class TankResourceParameters
    {
        public string TankShape { set; get; } = string.Empty;

        public string FrameType { set; get; } = string.Empty;

        // 蝶阀类型
        public string RadButterflyValveType { set; get; } = string.Empty;

        // 开关挖槽
        public string OltcIndent { set; get; } = string.Empty;

        // 高低压挖槽
        public string HVLVIndent { set; get; } = string.Empty;

        // 空气母线箱接口
        public string ActbDome { set; get; } = string.Empty;

        // 油油母线箱接口
        public string OctbDome { set; get; } = string.Empty;

        // 是否有小车
        public bool HasWheel { set; get; } = false;

        // 是否有中性点升高座
        public bool HasNeutralDome { set; get; } = false;

        // 侧壁1是否有鼓包
        public bool HasEnlargeSideWall1 { set; get; } = false;

        // 侧壁2是否有鼓包
        public bool HasEnlargeSideWall2 { set; get; } = false;

        // 侧壁3是否有鼓包
        public bool HasEnlargeSideWall3 { set; get; } = false;

        // 侧壁4是否有鼓包
        public bool HasEnlargeSideWall4 { set; get; } = false;
    }
}
