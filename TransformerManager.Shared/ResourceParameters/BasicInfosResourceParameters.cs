﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TransformerManager.Shared.ResourceParameters
{
    public class BasicInfosResourceParameters
    {
        public string ProjectNumber { set; get; } = string.Empty;

        public string ProjectName { set; get; } = string.Empty;

        public string ApplicationType { set; get; } = string.Empty;

        // 容量
        public double Capacity { set; get; } = -1;

        // 工频
        public int RatedFrequency { set; get; } = -1;

        // 连接组别
        public string VectorGroupSym { set; get; } = string.Empty;

        public double RatedHV { set; get; } = -1;

        public double RatedLV { set; get; } = -1;

        // 区域划分
        public string RegionType { set; get; } = string.Empty;

        // 产品标签
        public string ProdTag { set; get; } = string.Empty;

        // 防护方式
        public string Housing { set; get; } = string.Empty;

        // 绝缘油
        public string InsulationMaterial { set; get; } = string.Empty;

    }
}
