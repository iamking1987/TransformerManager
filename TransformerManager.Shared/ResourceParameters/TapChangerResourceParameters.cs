﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TransformerManager.Shared.ResourceParameters
{
    public class TapChangerResourceParameters
    {
        public string ProdType { set; get; } = string.Empty;

        public string TapChangerPosition { set; get; } = string.Empty;

        public string OperationType { set; get; } = string.Empty;

        public string Manufacturer { set; get; } = string.Empty;
    }
}
