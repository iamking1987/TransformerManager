﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TransformerManager.Shared.ResourceParameters
{
    public class AccessoryEquipmentResourceParameters
    {
        // 附件装备名称
        public string AccessoryEquName { set; get; } = string.Empty;

        // 产品型号
        public string ProdType { set; get; } = string.Empty;

        // 制造商
        public string Manufacturer { set; get; } = string.Empty;
    }
}
