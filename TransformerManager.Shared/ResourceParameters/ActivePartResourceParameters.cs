﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TransformerManager.Shared.ResourceParameters
{
    public class ActivePartResourceParameters
    {
        // 撑条数
        public int StripNumber { set; get; } = -1;

        // 垫块数
        public int SpacerNumber { set; get; } = -1;

        // 电压等级
        public int RatedVotage { set; get; } = -1;

        // 是否中部进线
        public bool IsCenterInfeed { set; get; } = false;

        // 是否全绝缘
        public bool IsUniformInsulation { set; get; } = false;

        // 线圈个数
        public int WindingNumber { set; get; } = -1;

        // 调压类型
        public string HVRType { set; get; } = string.Empty;

        // 最内线圈形式
        public string InnestWindingType { set; get; } = string.Empty;

        // 高压线圈AC
        public int HVAC { set; get; } = -1;

        // 是否有强油导向
        public bool HasOD { set; get; } = false;
    }
}
