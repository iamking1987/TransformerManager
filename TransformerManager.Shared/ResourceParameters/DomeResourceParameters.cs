﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TransformerManager.Shared.ResourceParameters
{
    public class DomeResourceParameters
    {
        public string DomeAssembleType { set; get; } = string.Empty;

        public double InnerDiameter { set; get; } = -1;
    }
}
