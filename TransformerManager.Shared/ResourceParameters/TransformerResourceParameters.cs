﻿namespace TransformerManager.Shared.ResourceParameters
{
    public class TransformerResourceParameters
    {
        public BasicInfosResourceParameters BasicInfos { set; get; }

        public TapChangerResourceParameters TapChanger { set; get; }

        public DomeResourceParameters Dome { set; get; }

        public BushingResourceParameters Bushing { set; get; }

        public TankResourceParameters Tank { set; get; }

        public ConservatorResourceParameters Conservator { set; get; }

        public CoolingSystemResourceParameters CoolingSystem { set; get; }

        public AccessoriesResourceParameters Accessories { set; get; }

        public AccessoryEquipmentResourceParameters AccesstoryEquipment { set; get; }

        public LeadResourceParameters Lead { set; get; }

        public LeadConnectorResourceParameters LeadConnector { set; get; }

        public WindingResourceParameters Winding { set; get; }

        public ActivePartResourceParameters ActivePart { set; get; }
    }
}
