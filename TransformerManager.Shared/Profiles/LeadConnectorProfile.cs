﻿using AutoMapper;
using TransformerManager.Shared.Dto.LeadConnector;
using TransformerManager.EF.Models;

namespace TransformerManager.Shared.Profiles
{
    public class LeadConnectorProfile : Profile
    {
        public LeadConnectorProfile()
        {
            CreateMap<LeadConnector, LeadConnectorDto>();

            CreateMap<LeadConnectorForCreationDto, LeadConnector>();

            CreateMap<LeadConnectorForUpdateDto, LeadConnector>();

            CreateMap<LeadConnectorDto, LeadConnectorForCreationDto>();

            CreateMap<LeadConnectorDto, LeadConnectorForUpdateDto>();
        }
    }
}
