﻿using AutoMapper;
using TransformerManager.Shared.Dto.ActivePart;
using TransformerManager.EF.Models;

namespace TransformerManager.Shared.Profiles
{
    public class ActivePartProfile : Profile
    {
        public ActivePartProfile()
        {
            CreateMap<ActivePart, ActivePartDto>();

            CreateMap<ActivePartForCreationDto, ActivePart>();

            CreateMap<ActivePartForUpdateDto, ActivePart>();

            CreateMap<ActivePartDto, ActivePartForCreationDto>();

            CreateMap<ActivePartDto, ActivePartForUpdateDto>();
        }
    }
}
