﻿using AutoMapper;
using System.Collections.Generic;
using TransformerManager.Shared.Dto.TapChanger;
using TransformerManager.EF.Models;

namespace TransformerManager.Shared.Profiles
{
    public class TapChangerProfile : Profile
    {
        public TapChangerProfile()
        {
            CreateMap<TapChanger, TapChangerDto>();

            CreateMap<TapChangerForCreationDto, TapChanger>();

            CreateMap<TapChangerForUpdateDto, TapChanger>();

            CreateMap<TapChangerDto, TapChangerForCreationDto>();

            CreateMap<TapChangerDto, TapChangerForUpdateDto>();
        }
    }
}
