﻿using AutoMapper;
using TransformerManager.Shared.Dto.AccessoryEquipment;
using TransformerManager.EF.Models;

namespace TransformerManager.Shared.Profiles
{
    public class AccessoryEquipmentProfile : Profile
    {
        public AccessoryEquipmentProfile()
        {
            CreateMap<AccessoryEquipment, AccessoryEquipmentDto>();

            CreateMap<AccessoryEquipmentForCreationDto, AccessoryEquipment>();

            CreateMap<AccessoryEquipmentForUpdateDto, AccessoryEquipment>();

            CreateMap<AccessoryEquipmentDto, AccessoryEquipmentForCreationDto>();

            CreateMap<AccessoryEquipmentDto, AccessoryEquipmentForUpdateDto>();
        }
    }
}
