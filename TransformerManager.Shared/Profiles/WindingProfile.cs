﻿using AutoMapper;
using TransformerManager.Shared.Dto;
using TransformerManager.Shared.Dto.Winding;
using TransformerManager.EF.Models;

namespace TransformerManager.Shared.Profiles
{
    public class WindingProfile : Profile
    {
        public WindingProfile()
        {
            CreateMap<Winding, WindingDto>();

            CreateMap<WindingForCreationDto, Winding>();

            CreateMap<WindingForUpdateDto, Winding>();

            CreateMap<WindingDto, WindingForCreationDto>();

            CreateMap<WindingDto, WindingForUpdateDto>();
        }
    }
}
