﻿using AutoMapper;
using TransformerManager.Shared.Dto.DiscWinding;
using TransformerManager.EF.Models;

namespace TransformerManager.Shared.Profiles
{
    public class DiscWindingProfile : Profile
    {
        public DiscWindingProfile()
        {
            CreateMap<DiscWinding, DiscWindingDto>();

            CreateMap<DiscWindingForCreationDto, DiscWinding>();

            CreateMap<DiscWindingForUpdateDto, DiscWinding>();

            CreateMap<DiscWindingDto, DiscWindingForCreationDto>();

            CreateMap<DiscWindingDto, DiscWindingForUpdateDto>();
        }
    }
}
