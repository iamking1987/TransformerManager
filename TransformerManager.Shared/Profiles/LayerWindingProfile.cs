﻿using AutoMapper;
using TransformerManager.Shared.Dto.LayerWinding;
using TransformerManager.EF.Models;

namespace TransformerManager.Shared.Profiles
{
    public class LayerWindingProfile : Profile
    {
        public LayerWindingProfile()
        {
            CreateMap<LayerWinding, LayerWindingDto>();

            CreateMap<LayerWindingForCreationDto, LayerWinding>();

            CreateMap<LayerWindingForUpdateDto, LayerWinding>();

            CreateMap<LayerWindingDto, LayerWindingForCreationDto>();

            CreateMap<LayerWindingDto, LayerWindingForUpdateDto>();
        }
    }
}
