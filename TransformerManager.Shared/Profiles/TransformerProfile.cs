﻿using AutoMapper;
using System;
using TransformerManager.Shared.Dto.Transformer;
using TransformerManager.EF.Models;

namespace TransformerManager.Shared.Profiles
{
    public class TransformerProfile : Profile
    {
        public TransformerProfile()
        {
            CreateMap<Transformer, TransformerDto>();

            CreateMap<TransformerForCreationDto, Transformer>()
                .ForMember(dest => dest.Id,
                opt => opt.MapFrom(src => Guid.NewGuid()));

            CreateMap<TransformerForUpdateDto, Transformer>();

            CreateMap<TransformerDto, TransformerForCreationDto>();

            CreateMap<TransformerDto, TransformerForUpdateDto>();

        }
    }
}
