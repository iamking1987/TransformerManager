﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TransformerManager.Shared.Dto.OperationLog;
using TransformerManager.EF.Models;

namespace TransformerManager.Shared.Profiles
{
    public class OperationLogsProfile : Profile
    {
        public OperationLogsProfile()
        {
            CreateMap<OperationLogs, OperationLogsDto>();
        }
    }
}
