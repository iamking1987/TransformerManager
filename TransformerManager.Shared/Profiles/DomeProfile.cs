﻿using AutoMapper;
using TransformerManager.Shared.Dto.Dome;
using TransformerManager.EF.Models;

namespace DomeManager.API.Profiles
{
    public class DomeProfile : Profile
    {
        public DomeProfile()
        {
            CreateMap<Dome, DomeDto>();

            CreateMap<DomeForCreationDto, Dome>();

            CreateMap<DomeForUpdateDto, Dome>();

            CreateMap<DomeDto, DomeForCreationDto>();

            CreateMap<DomeDto, DomeForUpdateDto>();
        }
    }
}
