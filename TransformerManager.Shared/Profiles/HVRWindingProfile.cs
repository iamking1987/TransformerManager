﻿using AutoMapper;
using TransformerManager.Shared.Dto.HVRWinding;
using TransformerManager.EF.Models;

namespace TransformerManager.Shared.Profiles
{
    public class HVRWindingProfile : Profile
    {
        public HVRWindingProfile()
        {
            CreateMap<HVRWinding, HVRWindingDto>();

            CreateMap<HVRWindingForCreationDto, HVRWinding>();

            CreateMap<HVRWindingForUpdateDto, HVRWinding>();

            CreateMap<HVRWindingDto, HVRWindingForCreationDto>();

            CreateMap<HVRWindingDto, HVRWindingForUpdateDto>();
        }
    }
}
