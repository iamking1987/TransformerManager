﻿using AutoMapper;
using TransformerManager.Shared.Dto.Tank;
using TransformerManager.EF.Models;

namespace TransformerManager.Shared.Profiles
{
    public class TankProfile : Profile
    {
        public TankProfile()
        {
            CreateMap<Tank, TankDto>();

            CreateMap<TankForCreationDto, Tank>();

            CreateMap<Tank, TankForUpdateDto>();

            CreateMap<TankForUpdateDto, Tank>();

            CreateMap<TankDto, TankForCreationDto>();

            CreateMap<TankDto, TankForUpdateDto>();
        }
    }
}
