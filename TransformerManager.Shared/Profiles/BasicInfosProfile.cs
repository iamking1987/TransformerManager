﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TransformerManager.Shared.Dto.BasicInfos;
using TransformerManager.EF.Models;

namespace TransformerManager.Shared.Profiles
{
    public class BasicInfosProfile : Profile
    {
        public BasicInfosProfile()
        {
            CreateMap<BasicInfos, BasicInfosDto>();

            CreateMap<BasicInfosForCreationDto, BasicInfos>();

            CreateMap<BasicInfosForUpdateDto, BasicInfos>();

            CreateMap<BasicInfosDto, BasicInfosForCreationDto>();

            CreateMap<BasicInfosDto, BasicInfosForUpdateDto>();
        }
    }
}
