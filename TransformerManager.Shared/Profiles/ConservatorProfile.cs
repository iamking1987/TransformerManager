﻿using AutoMapper;
using TransformerManager.Shared.Dto.Conservator;
using TransformerManager.EF.Models;

namespace TransformerManager.Shared.Profiles
{
    public class ConservatorProfile : Profile
    {
        public ConservatorProfile()
        {
            CreateMap<Conservator, ConservatorDto>();

            CreateMap<ConservatorForCreationDto, Conservator>();

            CreateMap<ConservatorForUpdateDto, Conservator>();

            CreateMap<ConservatorDto, ConservatorForCreationDto>();

            CreateMap<ConservatorDto, ConservatorForUpdateDto>();
        }
    }
}
