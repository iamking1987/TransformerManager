﻿using AutoMapper;
using TransformerManager.Shared.Dto.Lead;
using TransformerManager.EF.Models;

namespace TransformerManager.Shared.Profiles
{
    public class LeadProfile : Profile
    {
        public LeadProfile()
        {
            CreateMap<Lead, LeadDto>();

            CreateMap<LeadForCreationDto, Lead>();

            CreateMap<LeadForUpdateDto, Lead>();

            CreateMap<LeadDto, LeadForCreationDto>();

            CreateMap<LeadDto, LeadForUpdateDto>();
        }
    }
}
