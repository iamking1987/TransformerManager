﻿using AutoMapper;
using TransformerManager.Shared.Dto.CoolEquComp;
using TransformerManager.EF.Models;

namespace TransformerManager.Profiles
{
    public class CoolingSystemProfile : Profile
    {
        public CoolingSystemProfile()
        {
            CreateMap<CoolingSystem, CoolingSystemDto>();

            CreateMap<CoolingSystemForCreationDto, CoolingSystem>();

            CreateMap<CoolingSystemForUpdateDto, CoolingSystem>();

            CreateMap<CoolingSystemDto, CoolingSystemForCreationDto>();

            CreateMap<CoolingSystemDto, CoolingSystemForUpdateDto>();
        }
    }
}
