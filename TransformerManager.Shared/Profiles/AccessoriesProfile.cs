﻿using AutoMapper;
using TransformerManager.Shared.Dto.Accessories;
using TransformerManager.EF.Models;

namespace TransformerManager.Shared.Profiles
{
    public class AccessoriesProfile : Profile
    {
        public AccessoriesProfile()
        {
            CreateMap<Accessories, AccessoriesDto>();

            CreateMap<AccessoriesForCreationDto, Accessories>();

            CreateMap<AccessoriesForUpdateDto, Accessories>();

            CreateMap<AccessoriesDto, AccessoriesForCreationDto>();

            CreateMap<AccessoriesDto, AccessoriesForUpdateDto>();
        }
    }
}
