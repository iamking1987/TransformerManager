﻿using AutoMapper;
using TransformerManager.Shared.Dto.Bushing;
using TransformerManager.EF.Models;

namespace TransformerManager.Shared.Profiles
{
    public class BushingProfile : Profile
    {
        public BushingProfile()
        {
            CreateMap<Bushing, BushingDto>();

            CreateMap<BushingForCreationDto, Bushing>();

            CreateMap<BushingForUpdateDto, Bushing>();

            CreateMap<BushingDto, BushingForCreationDto>();

            CreateMap<BushingDto, BushingForUpdateDto>();
        }
    }
}
