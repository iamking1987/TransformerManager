﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransformerManager.Shared.Helper.HttpParams
{
    public class PaginationParams
    {
        public int TotalCount { set; get; }
        public int PageSize { set; get; }
        public int CurrentPage { set; get; }
        public int TotalPages { set; get; }
    }
}
