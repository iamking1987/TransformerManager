﻿using System.Net.Http.Headers;

namespace TransformerManager.Shared.Helper.HttpParams
{
    public class ResponseParams
    {
        public HttpResponseHeaders Headers { set; get; }

        public object Result { set; get; }

        public string Message { set; get; }

    }
}
