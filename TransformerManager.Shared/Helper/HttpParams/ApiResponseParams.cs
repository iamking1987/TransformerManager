﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace TransformerManager.Shared.Helper.HttpParams
{
    [DataContract]
    public class ApiResponseParams
    {
        public ApiResponseParams(object message, object result=null)
        {
            this.Message = message;
            this.Result = result;
        }

        [DataMember]
        public object Message { set; get; }
        
        [DataMember]
        public object Result { set; get; }

    }
}
