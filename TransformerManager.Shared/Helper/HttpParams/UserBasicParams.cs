﻿using System;
using System.Collections.Generic;

namespace TransformerManager.Shared.Helper.HttpParams
{
    public class UserBasicParams
    {
        public string UserName { set; get; }

        public string UserId { set; get; }

        public string RealNameEn { set; get; }

        public string RealNameCn { set; get; }

        public string Department { set; get; }

        public bool IsLeadEngineer { set; get; }

        public IEnumerable<String> Roles { set; get; }
    }
}
