﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransformerManager.Shared.Helper.HttpParams
{
    public class ErrorsParams
    {
        public object Errors { set; get; }

        public string Type { set; get; }

        public string Title { set; get; }

        public int Status { set; get; }

        public string Detail { set; get; }

        public string Instance { set; get; }

        public string TraceId { set; get; }
    }
}
