﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransformerManager.Shared.Helper.HttpParams
{
    public class ModifyPasswordParams
    {
        public string UserName { set; get; }
        public string CurrentPassword { set; get; }
        public string NewPassword { set; get; }
        public string ConfirmedNewPassword { set; get; }
    }
}
