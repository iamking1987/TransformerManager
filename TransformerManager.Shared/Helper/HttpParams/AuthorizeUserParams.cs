﻿namespace TransformerManager.Shared.Helper.HttpParams
{
    public class AuthorizeUserParams
    {
        public string UserName { set; get; }

        public string RoleName { set; get; }
    }
}
