﻿namespace TransformerManager.Shared.Helper.HttpParams
{
    public class TokenParams
    {
        public string AccessToken { set; get; }

        public string TokenType { set; get; }

        public int ExpiresIn { get; set; }   // add

        public string RefreshToken { get; set; }  // add
    }
}
