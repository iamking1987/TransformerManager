﻿using System;
using System.Collections.Generic;
using System.Text;
using TransformerManager.Shared.Dto.Transformer;

namespace TransformerManager.Shared.Helper.HttpParams
{
    public class TransformerParams
    {
        public UserBasicParams UserParam { set; get; }

        public TransformerDto TransformerDto { set; get; }
    }
}
