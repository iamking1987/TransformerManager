﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransformerManager.Shared.Helper.HttpParams
{
    public class RegisterParams
    {
        public string UserName { set; get; }

        public string RealNameEn { set; get; }

        public string RealNameCn { set; get; }

        public string Department { set; get; }

        public bool IsLeadEngnieer { set; get; }

        public string Password { set; get; }

        public string ConfirmPassword { set; get; }
    }
}
