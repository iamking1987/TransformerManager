﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransformerManager.Shared.Helper.HttpParams
{
    public class LoginParams
    {
        public string UserName { set; get; }
        public string Password { set; get; }
    }
}
