﻿namespace TransformerManager.Shared.Helper
{
    public enum ResourceUrlType
    {
        PreviousPage,
        NextPage
    }
}
