# TransformerManager

#### 介绍
TransformerManager 为STWH变压器数据管理系统，前后端均采用C#开发，前端框架为WPF的Prism，后端采用Asp.net Core 开发。

#### 软件架构
前端框架为WPF的Prism，后端采用Asp.net Core Web API开发， 基于RESTful API的风格.

#### 使用说明

1. 总览信息
![输入图片说明](https://images.gitee.com/uploads/images/2022/0221/092801_1cefdddf_7501339.png "pic01.png")

2. 项目详情
![输入图片说明](https://images.gitee.com/uploads/images/2022/0221/092920_b8a8619e_7501339.png "pic02.png")

3. 操作日志
![输入图片说明](https://images.gitee.com/uploads/images/2022/0221/092954_64019666_7501339.png "pic03.png")

4. 搜索
![输入图片说明](https://images.gitee.com/uploads/images/2022/0221/093121_f0f7fb4e_7501339.png "pic04.png")

5. 用户列表
![输入图片说明](https://images.gitee.com/uploads/images/2022/0221/093138_7fd117ec_7501339.png "pic05.png")



