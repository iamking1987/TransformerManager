﻿using Prism.Ioc;
using Prism.Modularity;
using TransformerManager.WPF.Common.ViewModels;
using TransformerManager.WPF.Common.Views;

namespace TransformerManager.WPF.Common
{
    public class CommonModule : IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {

        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {

        }
    }
}