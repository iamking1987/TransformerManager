﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using hc = HandyControl.Controls;

namespace TransformerManager.WPF.Common.Helper
{
    public class CheckComBoxHelper
    {
        static bool _isUpdating = false;

        public static readonly DependencyProperty SelectedItemsProperty =
            DependencyProperty.RegisterAttached("SelectedItems", typeof(ObservableCollection<string>), typeof(CheckComBoxHelper),
                new FrameworkPropertyMetadata(null, new PropertyChangedCallback(OnPropertyChanged)));

        public static ObservableCollection<string> GetSelectedItems(DependencyObject d)
        {
            return d.GetValue(SelectedItemsProperty) as ObservableCollection<string>;
        }

        public static void SetSelectedItems(DependencyObject d, ObservableCollection<string> value)
        {
            d.SetValue(SelectedItemsProperty, value);
        }

        private static void OnPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            hc.CheckComboBox checkComboBox = d as hc.CheckComboBox;

            if (!_isUpdating)
            {
                var result = e.NewValue as ObservableCollection<string>;
                checkComboBox.SelectedItems.Clear();
                if (result != null)
                {
                    foreach (var item in (ObservableCollection<string>)e.NewValue)
                    {
                        checkComboBox.SelectedItems.Add(item);
                    }
                }
                else
                {
                    checkComboBox.SelectedItems.Add(e.NewValue);
                }
            }
        }

        public static readonly DependencyProperty AttachProperty =
         DependencyProperty.RegisterAttached("Attach", typeof(bool), typeof(CheckComBoxHelper),
             new FrameworkPropertyMetadata(default(bool), new PropertyChangedCallback(OnAttached)));

        public static bool GetAttach(DependencyObject d)
        {
            return (bool)d.GetValue(AttachProperty);
        }

        public static void SetAttach(DependencyObject d, bool value)
        {
            d.SetValue(AttachProperty, value);
        }

        private static void OnAttached(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            hc.CheckComboBox checkComboBox = d as hc.CheckComboBox;
            checkComboBox.SelectionChanged += CheckComboxSelectionChanged;
        }

        private static void CheckComboxSelectionChanged(object sender, RoutedEventArgs e)
        {
            hc.CheckComboBox checkComboBox = sender as hc.CheckComboBox;
            _isUpdating = true;
            ObservableCollection<string> temp = new ObservableCollection<string>();
            foreach (var item in checkComboBox.SelectedItems)
            {
                temp.Add(item.ToString());
            }
            SetSelectedItems(checkComboBox, temp);
            _isUpdating = false;
        }
    }
}
