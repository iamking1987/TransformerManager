﻿using Prism.Events;

namespace TransformerManager.WPF.Common.Helper.Events
{
    public class ReloadXmlDataEnableEvent : PubSubEvent<bool>
    {
    }
}
