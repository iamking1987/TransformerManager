﻿using Prism.Events;
using System;
using System.Collections.Generic;
using System.Text;
using TransformerManager.Shared.Dto.Transformer;

namespace TransformerManager.WPF.Common.Helper.Events
{
    public class TransformerDtoEvent : PubSubEvent<TransformerDto>
    {
    }
}
