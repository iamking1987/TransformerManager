﻿using Prism.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace TransformerManager.WPF.Common.Helper.Events
{
    public class ReloadXmlDataPageEvent : PubSubEvent<string>
    {
    }
}
