﻿using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Text;
using TransformerManager.WPF.Common.Views;

namespace TransformerManager.WPF.Common.Helper
{
    public static class CustomiseRegionManager
    {
        /// <summary>
        /// 移除容器中已经加载了的变压器tab页面,主要用于清空上一次加载的数据
        /// </summary>
        /// <param name="navigationContext"></param>
        public static void RemoveRegions(NavigationContext navigationContext)
        {
            var regions = navigationContext.NavigationService.Region.RegionManager.Regions;

            if (regions.ContainsRegionWithName("BasicInfosRegion"))
                regions.Remove("BasicInfosRegion");

            if (regions.ContainsRegionWithName("TankInfoRegion"))
                regions.Remove("TankInfoRegion");

            if (regions.ContainsRegionWithName("ConservatorInfoRegion"))
                regions.Remove("ConservatorInfoRegion");

            if (regions.ContainsRegionWithName("CoolingSystemInfoRegion"))
                regions.Remove("CoolingSystemInfoRegion");
           
            if (regions.ContainsRegionWithName("AccessoriesInfoRegion"))
                regions.Remove("AccessoriesInfoRegion");

            if (regions.ContainsRegionWithName("LeadInfoRegion"))
                regions.Remove("LeadInfoRegion");

            if (regions.ContainsRegionWithName("WindingInfoRegion"))
                regions.Remove("WindingInfoRegion");

            if (regions.ContainsRegionWithName("ActivePartInfoRegion"))
                regions.Remove("ActivePartInfoRegion");
        }

        /// <summary>
        /// 预加载变压器信息的所有tab页面,避免多tab页面导航异常
        /// </summary>
        /// <param name="regionManager"></param>
        public static void PreLoadRegions(IRegionManager regionManager)
        {
            regionManager.RequestNavigate("BasicInfosRegion", nameof(BasicInfosTemplate));
            regionManager.RequestNavigate("TankInfoRegion", nameof(TankInfoTemplate));
            regionManager.RequestNavigate("ConservatorInfoRegion", nameof(ConservatorInfoTemplate));
            regionManager.RequestNavigate("CoolingSystemInfoRegion", nameof(CoolingSystemInfoTemplate));
            regionManager.RequestNavigate("AccessoriesInfoRegion", nameof(AccessoriesInfoTemplate));
            regionManager.RequestNavigate("LeadInfoRegion", nameof(LeadInfoTemplate));
            regionManager.RequestNavigate("WindingInfoRegion", nameof(WindingInfoTemplate));
            regionManager.RequestNavigate("ActivePartInfoRegion", nameof(ActivePartInfoTemplate));
        }
    }
}
