﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows.Data;

namespace TransformerManager.WPF.Common.Helper.Converter
{
    public class RoleListToStrConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var roles = value as List<string>;
            if (roles.Count == 0)
                return "Default";
            else
            {
                return string.Join("|", roles.ToArray());
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
