﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Windows.Data;

namespace TransformerManager.WPF.Common.Helper.Converter
{
    public class CheckComboBoxValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string chkCombValue = value == null ? string.Empty : value.ToString();

            if (!string.IsNullOrEmpty(chkCombValue.Trim()))
            {
                var array = chkCombValue.Split("|");
                return new ObservableCollection<string>(array);
            }
            return new ObservableCollection<string>();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            ObservableCollection<string> selectedValues = value as ObservableCollection<string>;
            if (selectedValues != null && selectedValues.Count != 0)
            {
                return string.Join("|", selectedValues);
            }

            return string.Empty;
        }
    }
}
