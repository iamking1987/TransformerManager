﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows.Data;

namespace TransformerManager.WPF.Common.Helper.Converter
{
    public class CheckBoxControlUserRolesValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            List<string> roles = new List<string>((IList<string>)value);
            if (roles.Count != 0 && roles.Contains("Admin"))
            {
                return true;
            }
            return false;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // 取反的原因是,点击ToggleButton时，其状态已经转换为最新的了
            if (!(bool)value)
            {
                return new List<string>() { "Admin" };
            }
            return new List<string>();
        }
    }
}
