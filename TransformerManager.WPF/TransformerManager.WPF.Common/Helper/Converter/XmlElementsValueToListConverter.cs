﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Windows.Data;
using System.Xml;
using System.Xml.Linq;

namespace TransformerManager.WPF.Common.Helper.Converter
{
    public class XmlElementsValueToListConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                ObservableCollection<string> xmlValues = new ObservableCollection<string>();
                var items = value as ICollection<XmlNode>;
                foreach (var item in items)
                {
                    XElement elem = XElement.Parse(item.OuterXml);
                    xmlValues.Add(elem.Element("ENName").Value);
                }
                return xmlValues;
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
