﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using TransformerManager.Shared.Dto.Tank;

namespace TransformerManager.WPF.Common.ViewModels
{
    public class TankInfoTemplateViewModel : BindableBase, INavigationAware
    {
        public TankInfoTemplateViewModel()
        {

        }

        #region 方法的定义

        private TankDto _tankDto;

        public TankDto TankDto
        {
            set
            {
                _tankDto = value;
                RaisePropertyChanged();
            }

            get
            {
                return _tankDto;
            }
        }

        #endregion


        #region 实现的接口

        public void OnNavigatedTo(NavigationContext navigationContext)
        {

        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {

        }

        #endregion
    }
}
