﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using TransformerManager.Shared.Dto.BasicInfos;
using TransformerManager.Shared.Dto.Bushing;
using TransformerManager.Shared.Dto.Dome;
using TransformerManager.Shared.Dto.TapChanger;

namespace TransformerManager.WPF.Common.ViewModels
{
    public class BasicInfosTemplateViewModel : BindableBase, INavigationAware
    {
        public BasicInfosTemplateViewModel()
        {
            InitAllCmd();
        }

        #region Command

        public DelegateCommand AddDomeCmd { private set; get; }

        public DelegateCommand DeleteDomeCmd { private set; get; }

        public DelegateCommand AddBushingCmd { private set; get; }

        public DelegateCommand DeleteBushingCmd { private set; get; }

        public DelegateCommand AddTapChangerCmd { private set; get; }

        public DelegateCommand DeleteTapChangerCmd { private set; get; }

        #endregion


        #region 方法的定义

        private BasicInfosDto _basicInfosDto;

        public BasicInfosDto BasicInfosDto
        {
            set
            {
                _basicInfosDto = value;
                RaisePropertyChanged();
            }

            get
            {
                return _basicInfosDto;
            }
        }

        /// <summary>
        /// 界面的数据表格中被选择到的Dome行
        /// </summary>
        private DomeDto _selectedDome
            = new DomeDto();
        public DomeDto SelectedDome
        {
            set
            {
                _selectedDome = value;

                this.RaisePropertyChanged();
            }
            get
            {
                return _selectedDome;
            }
        }

        /// <summary>
        /// 界面的数据表格中被选择到的Bushing行
        /// </summary>
        private BushingDto _selectedBushing
            = new BushingDto();
        public BushingDto SelectedBushing
        {
            set
            {
                _selectedBushing = value;

                this.RaisePropertyChanged();
            }
            get
            {
                return _selectedBushing;
            }
        }

        /// <summary>
        /// 界面的数据表格中被选择到的TapChanger行
        /// </summary>
        private TapChangerDto _selectedTapChanger
            = new TapChangerDto();
        public TapChangerDto SelectedTapChanger
        {
            set
            {
                _selectedTapChanger = value;

                this.RaisePropertyChanged();
            }
            get
            {
                return _selectedTapChanger;
            }
        }

        #endregion


        #region 函数的定义

        private void InitAllCmd()
        {
            AddDomeCmd = new DelegateCommand(AddDomeItem);

            DeleteDomeCmd = new DelegateCommand(DeleteDomeItem);

            AddBushingCmd = new DelegateCommand(AddBushingItem);

            DeleteBushingCmd = new DelegateCommand(DeleteBushingItem);

            AddTapChangerCmd = new DelegateCommand(AddTapChangerItem);

            DeleteTapChangerCmd = new DelegateCommand(DeleteTapChangerItem);

        }

        /// <summary>
        /// 在表格中添加一行升高座信息
        /// </summary>
        private void AddDomeItem()
        {
            var dome = new DomeDto()
            {
                DomeAssembleType = "Inversion",
                InnerDiameter = 0
            };

            if (_basicInfosDto.Domes == null)
            {
                _basicInfosDto.Domes = new ObservableCollection<DomeDto>();
            }

            BasicInfosDto.Domes.Add(dome);
        }

        /// <summary>
        /// 删除表格中的一行升高座信息
        /// </summary>
        private void DeleteDomeItem()
        {
            var dome = _basicInfosDto.Domes
                .FirstOrDefault(d => d.Id == _selectedDome.Id);

            BasicInfosDto.Domes.Remove(dome);
        }

        /// <summary>
        /// 在表格中添加一行套管信息
        /// </summary>
        private void AddBushingItem()
        {
            var bushing = new BushingDto()
            {
                InsulatorColor = "Brown",
                RefStandard = "GB/T4109",
                Manufacturer = "Zhida"
            };

            if (_basicInfosDto.Bushings == null)
            {
                _basicInfosDto.Bushings = new ObservableCollection<BushingDto>();
            }

            BasicInfosDto.Bushings.Add(bushing);
        }

        /// <summary>
        /// 删除表格中的一行套管信息
        /// </summary>
        private void DeleteBushingItem()
        {
            var bushing = _basicInfosDto.Bushings
                .FirstOrDefault(b => b.Id == _selectedBushing.Id);

            BasicInfosDto.Bushings.Remove(bushing);
        }

        /// <summary>
        /// 在表格中添加一行开关信息
        /// </summary>
        private void AddTapChangerItem()
        {
            var tapChanger = new TapChangerDto()
            {
                ProdType = string.Empty,
                TapChangerPosition = "LeftSide",
                OperationType = "OLTC",
                Manufacturer = "MR"
            };

            if (_basicInfosDto.TapChangers == null)
            {
                _basicInfosDto.TapChangers = new ObservableCollection<TapChangerDto>();
            }

            BasicInfosDto.TapChangers.Add(tapChanger);
        }

        /// <summary>
        /// 删除表格中的一行开关信息
        /// </summary>
        private void DeleteTapChangerItem()
        {
            var tapChanger = _basicInfosDto.TapChangers
                .FirstOrDefault(b => b.Id == _selectedTapChanger.Id);

            BasicInfosDto.TapChangers.Remove(tapChanger);
        }

        #endregion


        #region 实现的接口

        public void OnNavigatedTo(NavigationContext navigationContext)
        {

        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {

        }

        #endregion
    }
}
