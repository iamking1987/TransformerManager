﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using TransformerManager.Shared.Dto.Accessories;
using TransformerManager.Shared.Dto.AccessoryEquipment;

namespace TransformerManager.WPF.Common.ViewModels
{
    public class AccessoriesInfoTemplateViewModel : BindableBase, INavigationAware
    {
        public AccessoriesInfoTemplateViewModel()
        {
            InitAllCmd();
        }

        #region Command

        public DelegateCommand AddAccEquCmd { private set; get; }

        public DelegateCommand DeleteAccEquCmd { private set; get; }

        #endregion

        #region 方法的定义

        private AccessoriesDto _accessoriesDto;

        public AccessoriesDto AccessoriesDto
        {
            set
            {
                _accessoriesDto = value;
                RaisePropertyChanged();
            }

            get
            {
                return _accessoriesDto;
            }
        }

        /// <summary>
        /// 界面的数据表格中被选择到的AccessoryEqu行
        /// </summary>
        private AccessoryEquipmentDto _selectedAccessoryEqu
            = new AccessoryEquipmentDto();
        public AccessoryEquipmentDto SelectedAccessoryEqu
        {
            set
            {
                _selectedAccessoryEqu = value;

                this.RaisePropertyChanged();
            }
            get
            {
                return _selectedAccessoryEqu;
            }
        }

        #endregion

        #region 函数的定义

        private void InitAllCmd()
        {
            AddAccEquCmd = new DelegateCommand(AddAccessoryEquItem);

            DeleteAccEquCmd = new DelegateCommand(DeleteAccessoryEquItem);
        }

        /// <summary>
        /// 在表格中添加一附件装备信息
        /// </summary>
        private void AddAccessoryEquItem()
        {
            var accessoryEqu = new AccessoryEquipmentDto()
            {
                AccessoryEquName = "SurgeArrester",
                ProdType = string.Empty,
                Manufacturer = string.Empty
            };

            if (_accessoriesDto.AccesstoryEquipments == null)
            {
                _accessoriesDto.AccesstoryEquipments = new ObservableCollection<AccessoryEquipmentDto>();
            }

            AccessoriesDto.AccesstoryEquipments.Add(accessoryEqu);
        }

        /// <summary>
        /// 删除表格中的一行开关信息
        /// </summary>
        private void DeleteAccessoryEquItem()
        {
            var accesstoryEqu = _accessoriesDto.AccesstoryEquipments
                .FirstOrDefault(b => b.Id == _selectedAccessoryEqu.Id);

            AccessoriesDto.AccesstoryEquipments.Remove(accesstoryEqu);
        }

        #endregion

        #region 实现的接口

        public void OnNavigatedTo(NavigationContext navigationContext)
        {

        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {

        }

        #endregion
    }
}
