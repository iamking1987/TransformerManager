﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TransformerManager.WPF.Common.ViewModels
{
    public class AboutDialogViewModel : BindableBase, IDialogAware
    {
        public AboutDialogViewModel()
        {
            CloseCmd = new DelegateCommand(() => OnDialogClosed());
        }

        public DelegateCommand CloseCmd { set; get; }

        public string Title => string.Empty;

        public event Action<IDialogResult> RequestClose;

        public bool CanCloseDialog()
        {
            return true;
        }

        public void OnDialogClosed()
        {
            RequestClose?.Invoke(new DialogResult());
        }

        public void OnDialogOpened(IDialogParameters parameters)
        {
            
        }
    }
}
