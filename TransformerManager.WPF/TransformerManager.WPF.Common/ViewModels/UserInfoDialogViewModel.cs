﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using System;
using TransformerManager.Shared.Helper.HttpParams;

namespace TransformerManager.WPF.Common.ViewModels
{
    public class UserInfoDialogViewModel : BindableBase, IDialogAware
    {

        public string Title => string.Empty;
        public UserInfoDialogViewModel()
        {
            InitCmd();
        }

        #region Command的定义

        public DelegateCommand YesCmd { set; get; }

        public DelegateCommand NoCmd { set; get; }

        #endregion


        #region 事件的定义

        public event Action<IDialogResult> RequestClose;

        #endregion


        #region 方法的定义

        private UserBasicParams _currentUserInfo;
        public UserBasicParams CurrentUserInfo
        {
            set
            {
                _currentUserInfo = value;

                RaisePropertyChanged();
            }

            get
            {
                return _currentUserInfo;
            }
        }

        #endregion


        #region 函数的定义

        private void InitCmd()
        {
            YesCmd = new DelegateCommand(() =>
            {
                OnDialogClosed();
            });

            NoCmd = new DelegateCommand(() =>
            {
                RequestClose?.Invoke(new DialogResult(ButtonResult.No));
            });
        }

        #endregion


        #region 接口的实现

        public bool CanCloseDialog()
        {
            return true;
        }

        public void OnDialogClosed()
        {
            var callbackParams = new DialogParameters() { { "CurrentUserInfo", _currentUserInfo } };

            RequestClose?.Invoke(new DialogResult(ButtonResult.OK, callbackParams));
        }

        public void OnDialogOpened(IDialogParameters parameters)
        {
            CurrentUserInfo = parameters.GetValue<UserBasicParams>("UserInfo");
        }

        #endregion
    }
}
