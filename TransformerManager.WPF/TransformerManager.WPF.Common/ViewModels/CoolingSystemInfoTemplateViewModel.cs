﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using TransformerManager.Shared.Dto.CoolEquComp;

namespace TransformerManager.WPF.Common.ViewModels
{
    public class CoolingSystemInfoTemplateViewModel : BindableBase, INavigationAware
    {
        private readonly IDialogService _dialogService;
        public CoolingSystemInfoTemplateViewModel(IDialogService dialogService)
        {
            _dialogService = dialogService;

            InitCmd();
        }

        #region command

        public DelegateCommand RadiatorCmd { set; get; }

        #endregion

        #region 方法的定义

        private CoolingSystemDto _coolingSystemDto
            = new CoolingSystemDto();

        public CoolingSystemDto CoolingSystemDto
        {
            set
            {
                _coolingSystemDto = value;
                RaisePropertyChanged();
            }

            get
            {
                return _coolingSystemDto;
            }
        }

        #endregion

        #region 函数的定义

        private void InitCmd()
        {
            RadiatorCmd = new DelegateCommand(EditRadiatorType);
        }

        private void EditRadiatorType()
        {
            ObservableCollection<string> radTypeCollection;
            if(string.IsNullOrEmpty(_coolingSystemDto.RadiatorType))
            {
                radTypeCollection = new ObservableCollection<string>();
            }
            else
            {
                string[] radTypeArray = _coolingSystemDto.RadiatorType.Split(",");
                radTypeCollection = new ObservableCollection<string>(radTypeArray);
            }

            DialogParameters dialogParams = new DialogParameters()
            {
                {"RadiatorTypes", radTypeCollection}
            };

            string radTypeStr = string.Empty;
            _dialogService.ShowDialog("RadiatorEditDialog", dialogParams, r =>
            {
                if(r.Result == ButtonResult.OK)
                {
                    CoolingSystemDto.RadiatorType = 
                        r.Parameters.GetValue<string>("UpdatedRadTypesStr");
                    RaisePropertyChanged("CoolingSystemDto");
                }
            });
        }

        #endregion


        #region 实现的接口

        public void OnNavigatedTo(NavigationContext navigationContext)
        {

        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {

        }

        #endregion
    }
}
