﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using TransformerManager.Shared.Dto;
using TransformerManager.Shared.Dto.DiscWinding;
using TransformerManager.Shared.Dto.HVRWinding;
using TransformerManager.Shared.Dto.LayerWinding;

namespace TransformerManager.WPF.Common.ViewModels
{
    public class WindingInfoTemplateViewModel : BindableBase, INavigationAware
    {
        public WindingInfoTemplateViewModel()
        {
            InitAllCmd();
        }

        #region Command

        public DelegateCommand AddHVRWindingCmd { private set; get; }

        public DelegateCommand DeleteHVRWindingCmd { private set; get; }

        public DelegateCommand AddDiscWindingCmd { private set; get; }

        public DelegateCommand DeleteDiscWindingCmd { private set; get; }

        public DelegateCommand AddLayerWindingCmd { private set; get; }

        public DelegateCommand DeleteLayerWindingCmd { private set; get; }
        #endregion


        #region 方法的定义

        private WindingDto _windingDto;

        public WindingDto WindingDto
        {
            set
            {
                _windingDto = value;
                RaisePropertyChanged();
            }

            get
            {
                return _windingDto;
            }
        }

        /// <summary>
        /// 界面的数据表格中被选择到的HVRWinding行
        /// </summary>
        private HVRWindingDto _selectedHVRWinding
            = new HVRWindingDto();
        public HVRWindingDto SelectedHVRWinding
        {
            set
            {
                _selectedHVRWinding = value;

                this.RaisePropertyChanged();
            }
            get
            {
                return _selectedHVRWinding;
            }
        }

        /// <summary>
        /// 界面的数据表格中被选择到的DiscWinding行
        /// </summary>
        private DiscWindingDto _selectedDiscWinding
            = new DiscWindingDto();
        public DiscWindingDto SelectedDiscWinding
        {
            set
            {
                _selectedDiscWinding = value;

                this.RaisePropertyChanged();
            }
            get
            {
                return _selectedDiscWinding;
            }
        }

        /// <summary>
        /// 界面的数据表格中被选择到的LayerWinding行
        /// </summary>
        private LayerWindingDto _selectedLayerWinding
            = new LayerWindingDto();
        public LayerWindingDto SelectedLayerWinding
        {
            set
            {
                _selectedLayerWinding = value;

                this.RaisePropertyChanged();
            }
            get
            {
                return _selectedLayerWinding;
            }
        }

        #endregion

        #region 函数定义

        private void InitAllCmd()
        {
            AddHVRWindingCmd = new DelegateCommand(AddHVRWindingItem);

            DeleteHVRWindingCmd = new DelegateCommand(DeleteHVRWindingItem);

            AddDiscWindingCmd = new DelegateCommand(AddDiscWindingItem);

            DeleteDiscWindingCmd = new DelegateCommand(DeleteDiscWindingItem);

            AddLayerWindingCmd = new DelegateCommand(AddLayerWindingItem);

            DeleteLayerWindingCmd = new DelegateCommand(DeleteLayerWindingItem);
        }

        /// <summary>
        /// 在表格中添加一HVRWinding信息
        /// </summary>
        private void AddHVRWindingItem()
        {
            var hvrWinding = new HVRWindingDto()
            {
                HVRWindingType = "W27",
                WindingDirection = "Left",
                StripNumber = 16,
                SpacerNumber = 0,
                SpacerWidth = 0,
                ConductorNumber = 2,
                HasFSA = false,
                IsUniformInsulation = false,
                IsCTC = false,
                HVRStepNumber = 4,
                TopKsNumber = 2,
                AngleRingNumber = 0,
                IsDivided = false
            };

            if (_windingDto.HVRWindings == null)
            {
                _windingDto.HVRWindings = new ObservableCollection<HVRWindingDto>();
            }

            WindingDto.HVRWindings.Add(hvrWinding);
        }

        /// <summary>
        /// 删除表格中的一行HVRWinding信息
        /// </summary>
        private void DeleteHVRWindingItem()
        {
            var hvrWinding = _windingDto.HVRWindings
                .FirstOrDefault(b => b.Id == _selectedHVRWinding.Id);

            WindingDto.HVRWindings.Remove(hvrWinding);
        }

        /// <summary>
        /// 在表格中添加一DiscWinding信息
        /// </summary>
        private void AddDiscWindingItem()
        {
            var discWinding = new DiscWindingDto()
            {
                WindingName = "HV",
                DiscWindingType = "Disc",
                WindingDirection = "Left",
                StripNumber = 16,
                SpacerNumber = 0,
                SpacerWidth = 0,
                ConductorNumber = 1,
                HasFSA = false,
                OilDeflectorNumber = 1,
                IsUniformInsulation = false,
                IsCTC = false,
                HasBananaSpacer = false,
                HasSecondDiscOut = false,
                IsSandwichInterleaved = false,
                IsInterleaved = false,
                ConductorStrandNumber = 1,
                IsDivided = false,
                HasLeadingPlate = false
            };

            if (_windingDto.DiscWindings == null)
            {
                _windingDto.DiscWindings = new ObservableCollection<DiscWindingDto>();
            }

            WindingDto.DiscWindings.Add(discWinding);
        }

        /// <summary>
        /// 删除表格中的一行DiscWinding信息
        /// </summary>
        private void DeleteDiscWindingItem()
        {
            var discWinding = _windingDto.DiscWindings
                .FirstOrDefault(b => b.Id == _selectedDiscWinding.Id);

            WindingDto.DiscWindings.Remove(discWinding);
        }

        /// <summary>
        /// 在表格中添加一引线连接件信息
        /// </summary>
        private void AddLayerWindingItem()
        {
            var layerWinding = new LayerWindingDto()
            {
                WindingName = "HV",
                LayerWindingType = "Disc",
                InnerLayerDirection = "Left",
                StripNumber = 16,
                SpacerNumber = 0,
                SpacerWidth = 0,
                AsialConductorNumber = 1,
                RadialConductorNumber = 1,
                HasFSA = false,
                OilDeflectorNumber = 1,
                IsUniformInsulation = false,
                InnerCylinderNumber = 1,
                HasCoreShielding = false,
                IsDivided = false,
                IsLever = false
            };

            if (_windingDto.LayerWindings == null)
            {
                _windingDto.LayerWindings = new ObservableCollection<LayerWindingDto>();
            }

            WindingDto.LayerWindings.Add(layerWinding);
        }

        /// <summary>
        /// 删除表格中的一行LayerWinding信息
        /// </summary>
        private void DeleteLayerWindingItem()
        {
            var layerWinding = _windingDto.LayerWindings
                .FirstOrDefault(b => b.Id == _selectedLayerWinding.Id);

            WindingDto.LayerWindings.Remove(layerWinding);
        }

        #endregion


        #region 实现的接口

        public void OnNavigatedTo(NavigationContext navigationContext)
        {

        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {

        }

        #endregion
    }
}
