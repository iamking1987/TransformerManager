﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using TransformerManager.Shared.Helper.HttpParams;

namespace TransformerManager.WPF.Common.ViewModels
{
    public class UserListDialogViewModel : BindableBase, IDialogAware
    {
        public UserListDialogViewModel()
        {
            InitCmd();
        }

        #region Command的定义

        public DelegateCommand YesCmd { set; get; }

        public DelegateCommand NoCmd { set; get; }

        #endregion


        #region 方法的定义

        public string Title => "主设列表";

        private ObservableCollection<UserBasicParams> _leadEngineers
            = new ObservableCollection<UserBasicParams>();
        public ObservableCollection<UserBasicParams> LeadEngineers
        {
            set
            {
                _leadEngineers = value;

                RaisePropertyChanged();
            }
            get
            {
                return _leadEngineers;
            }
        }

        private UserBasicParams _selectedUser =
            new UserBasicParams();
        public UserBasicParams SelectedUser
        {
            set
            {
                _selectedUser = value;

                RaisePropertyChanged();
            }

            get
            {
                return _selectedUser;
            }
        }

        #endregion


        #region 事件的定义

        public event Action<IDialogResult> RequestClose;

        #endregion


        #region 函数的定义

        private void InitCmd()
        {
            YesCmd = new DelegateCommand(() =>
            {
                OnDialogClosed();
            });

            NoCmd = new DelegateCommand(() =>
            {
                RequestClose?.Invoke(new DialogResult(ButtonResult.No));
            });
        }

        #endregion


        #region 接口的实现

        public bool CanCloseDialog()
        {
            return true;
        }

        public void OnDialogClosed()
        {
            var callbackParams = new DialogParameters() { { "SelectedUser", SelectedUser } };

            RequestClose?.Invoke(new DialogResult(ButtonResult.OK, callbackParams));
        }

        public void OnDialogOpened(IDialogParameters parameters)
        {
            LeadEngineers = parameters.GetValue<ObservableCollection<UserBasicParams>>("UserList");

        }

        #endregion
    }
}
