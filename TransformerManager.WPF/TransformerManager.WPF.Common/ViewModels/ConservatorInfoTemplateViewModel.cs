﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using TransformerManager.Shared.Dto.Conservator;

namespace TransformerManager.WPF.Common.ViewModels
{
    public class ConservatorInfoTemplateViewModel : BindableBase, INavigationAware
    {
        public ConservatorInfoTemplateViewModel()
        {

        }

        #region 方法的定义

        private ConservatorDto _conservatorDto;

        public ConservatorDto ConservatorDto
        {
            set
            {
                _conservatorDto = value;
                RaisePropertyChanged();
            }

            get
            {
                return _conservatorDto;
            }
        }

        #endregion


        #region 实现的接口

        public void OnNavigatedTo(NavigationContext navigationContext)
        {

        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {

        }

        #endregion

    }
}
