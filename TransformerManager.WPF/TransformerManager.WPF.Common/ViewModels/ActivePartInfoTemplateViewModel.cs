﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using TransformerManager.Shared.Dto.ActivePart;

namespace TransformerManager.WPF.Common.ViewModels
{
    public class ActivePartInfoTemplateViewModel : BindableBase, INavigationAware
    {
        public ActivePartInfoTemplateViewModel()
        {

        }

        #region 方法的定义

        private ActivePartDto _activePartDto;

        public ActivePartDto ActivePartDto
        {
            set
            {
                _activePartDto = value;
                RaisePropertyChanged();
            }

            get
            {
                return _activePartDto;
            }
        }

        #endregion


        #region 实现的接口

        public void OnNavigatedTo(NavigationContext navigationContext)
        {

        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {

        }

        #endregion
    }
}
