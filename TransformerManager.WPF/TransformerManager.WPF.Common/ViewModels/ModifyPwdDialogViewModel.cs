﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using System;

namespace TransformerManager.WPF.Common.ViewModels
{
    public class ModifyPwdDialogViewModel : BindableBase, IDialogAware
    {
        public ModifyPwdDialogViewModel()
        {
            InitCmd();
        }

        #region Command的定义

        public DelegateCommand YesCmd { set; get; }

        public DelegateCommand NoCmd { set; get; }

        #endregion


        #region 方法的定义
        public string Title => "修改密码";

        private string _currentPwd = string.Empty;
        public string CurrentPwd
        {
            set
            {
                _currentPwd = value;
                RaisePropertyChanged();
            }
            get
            {
                return _currentPwd;
            }
        }

        private string _newPwd = string.Empty;
        public string NewPwd
        {
            set
            {
                _newPwd = value;
                RaisePropertyChanged();
            }
            get
            {
                return _newPwd;
            }
        }

        private string _confirmPwd = string.Empty;
        public string ConfirmPwd
        {
            set
            {
                _confirmPwd = value;
                RaisePropertyChanged();
            }
            get
            {
                return _confirmPwd;
            }
        }

        #endregion


        #region 事件的定义

        public event Action<IDialogResult> RequestClose;

        #endregion


        #region 函数的定义

        private void InitCmd()
        {
            YesCmd = new DelegateCommand(() =>
            {
                OnDialogClosed();
            });

            NoCmd = new DelegateCommand(() =>
            {
                RequestClose?.Invoke(new DialogResult(ButtonResult.No));
            });
        }

        #endregion


        #region 接口的实现

        public bool CanCloseDialog()
        {
            return true;
        }

        public void OnDialogClosed()
        {
            var callbackParams = new DialogParameters()
            {
                { "CurrentPwd", CurrentPwd },
                { "NewPwd", NewPwd },
                { "ConfirmPwd", ConfirmPwd }
            };

            RequestClose?.Invoke(new DialogResult(ButtonResult.OK, callbackParams));
        }

        public void OnDialogOpened(IDialogParameters parameters)
        {

        }

        #endregion
    }
}
