﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using TransformerManager.Shared.Dto.Lead;
using TransformerManager.Shared.Dto.LeadConnector;

namespace TransformerManager.WPF.Common.ViewModels
{
    public class LeadInfoTemplateViewModel : BindableBase
    {
        public LeadInfoTemplateViewModel()
        {
            InitAllCmd();
        }

        #region Command

        public DelegateCommand AddLeadConnCmd { private set; get; }

        public DelegateCommand DeleteLeadConnCmd { private set; get; }

        #endregion


        #region 方法的定义

        private LeadDto _leadDto;

        public LeadDto LeadDto
        {
            set
            {
                _leadDto = value;
                RaisePropertyChanged();
            }

            get
            {
                return _leadDto;
            }
        }

        /// <summary>
        /// 界面的数据表格中被选择到的LeadConn行
        /// </summary>
        private LeadConnectorDto _selectedLeadConn
            = new LeadConnectorDto();
        public LeadConnectorDto SelectedLeadConn
        {
            set
            {
                _selectedLeadConn = value;

                this.RaisePropertyChanged();
            }
            get
            {
                return _selectedLeadConn;
            }
        }

        #endregion


        #region 函数的定义

        private void InitAllCmd()
        {
            AddLeadConnCmd = new DelegateCommand(AddLeadConnItem);

            DeleteLeadConnCmd = new DelegateCommand(DeleteLeadConnItem);
        }

        /// <summary>
        /// 在表格中添加一引线连接件信息
        /// </summary>
        private void AddLeadConnItem()
        {
            var leadConn = new LeadConnectorDto()
            {
                LeadName = "HV",
                LeadType = "Cable",
                LeadDimensionType = "S95",
                Si = 0.3
            };

            if (_leadDto.LeadConnectors == null)
            {
                _leadDto.LeadConnectors = new ObservableCollection<LeadConnectorDto>();
            }

            LeadDto.LeadConnectors.Add(leadConn);
        }

        /// <summary>
        /// 删除表格中的一行引线连接件信息
        /// </summary>
        private void DeleteLeadConnItem()
        {
            var leadConn = _leadDto.LeadConnectors
                .FirstOrDefault(c => c.Id == _selectedLeadConn.Id);

            LeadDto.LeadConnectors.Remove(leadConn);
        }

        #endregion

        #region 实现的接口

        public void OnNavigatedTo(NavigationContext navigationContext)
        {

        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {

        }

        #endregion
    }
}
