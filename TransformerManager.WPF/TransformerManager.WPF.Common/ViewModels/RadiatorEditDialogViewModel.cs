﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using System;
using System.Collections.ObjectModel;
using System.Windows;
using TransformerManager.WPF.Common.Utils;
using hc = HandyControl.Controls;

namespace TransformerManager.WPF.Common.ViewModels
{
    public class RadiatorEditDialogViewModel : BindableBase, IDialogAware
    {
        public RadiatorEditDialogViewModel()
        {
            InitCmd();
        }

        public string Title => "片散型号编辑";

        public event Action<IDialogResult> RequestClose;

        #region Command的定义

        public DelegateCommand YesCmd { set; get; }

        public DelegateCommand NoCmd { set; get; }
        public DelegateCommand DeleteElemCmd { set; get; }

        public DelegateCommand AddCmd { set; get; }

        #endregion

        #region 属性定义

        private ObservableCollection<string> radTypes
            = new ObservableCollection<string>();
        public ObservableCollection<string> RadTypes
        {
            set
            {
                radTypes = value;
                RaisePropertyChanged();
            }
            get
            {
                return radTypes;
            }
        }

        private RadiatorType addedItem
            = new RadiatorType();
        public RadiatorType AddedItem
        {
            set
            {
                addedItem = value;
                RaisePropertyChanged();
            }
            get
            {
                return addedItem;
            }
        }

        private string selectedItem;
        public string SelectedItem
        {
            set
            {
                selectedItem = value;
                RaisePropertyChanged();
            }
            get
            {
                return selectedItem;
            }
        }

        #endregion

        #region 函数的定义

        private void InitCmd()
        {
            YesCmd = new DelegateCommand(() =>
            {
                var result = hc.MessageBox.Show(
                    "是否保存编辑?",
                    "保存?",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    OnDialogClosed();
                }
            });

            NoCmd = new DelegateCommand(() =>
            {
                RequestClose?.Invoke(new DialogResult(ButtonResult.No));
            });

            DeleteElemCmd = new DelegateCommand(() =>
            {
                if (selectedItem == null) return;

                bool isSuccess = RadTypes.Remove(selectedItem);

                if (!isSuccess)
                {
                    hc.Growl.Error($"响应信息：错误!删除操作失败!", "Notification");
                }
            });

            AddCmd = new DelegateCommand(() => {
                if (addedItem == null) return;

                RadTypes.Add($"PC{addedItem.RadHeight}-{addedItem.RadNumber}/{addedItem.RadWidth}");
            });
        }

        #endregion

        #region 接口的实现

        public bool CanCloseDialog()
        {
            return true;
        }

        public void OnDialogClosed()
        {
            DialogParameters dialogParams = new DialogParameters()
            {
                { "UpdatedRadTypesStr", string.Join(",", radTypes)}
            };
            RequestClose?.Invoke(new DialogResult(ButtonResult.OK, dialogParams));
        }

        public void OnDialogOpened(IDialogParameters parameters)
        {
            var dialogInParams = parameters
                .GetValue<ObservableCollection<string>>("RadiatorTypes");
            if (dialogInParams != null)
            {
                RadTypes = dialogInParams;
            }
        }

        #endregion
    }
}
