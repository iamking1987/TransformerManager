﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TransformerManager.Shared.Dto.Transformer;
using TransformerManager.Shared.Helper.HttpParams;
using TransformerManager.Shared.ResourceParameters;

namespace TransformerManager.WPF.Common.Http.Service
{
    public interface ITransformerService
    {
        Task<ResponseParams> GetTransformersAsync(
            TransformerResourceParameters transformerResource,
            int pageIndex,
            int pageSize);

        Task<ResponseParams> DeleteTransformerByIdAsync(Guid Id);

        Task<ResponseParams> GetTransformerInfoById(Guid Id);

        Task<ResponseParams> UpdateTransformerInfoByIdAsync(Guid Id, TransformerDto transformerDto);

        Task<ResponseParams> AddTransformerAsync(TransformerDto transformerDto);

        Task<ResponseParams> CheckProjectNumberValidAsync(string projectNumber);

        Task<int> GetTransformersNumByRegionAsync(string region, DateTime from, DateTime end);

    }
}
