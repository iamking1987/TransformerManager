﻿using Newtonsoft.Json;
using System;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading.Tasks;
using TransformerManager.Shared.Helper.HttpParams;

namespace TransformerManager.WPF.Common.Http.Service
{
    public class UserInfoService : IUserInfoService
    {
        private readonly HttpRequest _httpRequest;
        public UserInfoService(HttpRequest httpRequest)
        {
            _httpRequest = httpRequest;
        }

        public async Task<UserBasicParams> GetUserInfoByIdAsync(string id)
        {
            string url = $"Account/userId/{id}";
            var responseParams = await _httpRequest.RequestAsync(HttpMethod.Get, url, null);
            UserBasicParams userInfo;
            if (responseParams.Result != null)
            {
                userInfo = JsonConvert.DeserializeObject<UserBasicParams>(responseParams.Result.ToString());
            }
            else
            {
                userInfo = null;
            }
            return userInfo;
        }

        public async Task<UserBasicParams> GetUserInfoByUserNameAsync(string userName)
        {
            string url = $"Account/username/{userName}";
            var responseParams = await _httpRequest.RequestAsync(HttpMethod.Get, url, null);
            UserBasicParams userInfo;
            if (responseParams.Result != null)
            {
                userInfo = JsonConvert.DeserializeObject<UserBasicParams>(responseParams.Result.ToString());
            }
            else
            {
                userInfo = null;
            }
            return userInfo;
        }

        public async Task<ResponseParams> GetUsersAsync(string keywords, bool onlyLeadEngineer, int pageIndex, int pageSize)
        {
            string url = $"Account/users?keywords={keywords}&onlyLeadEngineer={onlyLeadEngineer}&PageNumber={pageIndex}&PageSize={pageSize}";
            return await _httpRequest.RequestAsync(HttpMethod.Get, url, null);
        }

        public async Task ModifyPasswordAsync(ModifyPasswordParams modifyPasswordParams)
        {
            string url = $"Account/pwdchange";
            string reqBody = JsonConvert.SerializeObject(modifyPasswordParams);
            await _httpRequest.RequestAsync(HttpMethod.Post, url, null, reqBody);
        }

        public async Task<UserBasicParams> ModifyUserInfoAsync(UserBasicParams userBasicParams)
        {
            string url = $"Account/userinfo";
            string reqBody = JsonConvert.SerializeObject(new 
            {
                userBasicParams.UserId,
                userBasicParams.UserName,
                userBasicParams.RealNameCn,
                userBasicParams.RealNameEn,
                userBasicParams.Department,
                userBasicParams.IsLeadEngineer,
            });
            var responseParams = await _httpRequest.RequestAsync(HttpMethod.Post, url, null, reqBody);

            UserBasicParams userInfo;
            if (responseParams.Result != null)
            {
                userInfo = JsonConvert.DeserializeObject<UserBasicParams>(responseParams.Result.ToString());
            }
            else
            {
                userInfo = null;
            }
            return userInfo;
        }

        public async Task ResetUserPasswordAsync(string userName)
        {
            string url = $"Account/pwdreset/{userName}";
            await _httpRequest.RequestAsync(HttpMethod.Post, url, null);
        }

        public async Task AuthorizeRole(string userName)
        {
            string url = "Account/authorize";
            var authorizeInfo = new
            {
                UserName = userName,
                RoleName = "Admin"
            };
            var reqBody = JsonConvert.SerializeObject(authorizeInfo);
            await _httpRequest.RequestAsync(HttpMethod.Post, url, null, reqBody);
        }

        public async Task UnauthorizeRole(string userName)
        {
            string url = "Account/unauthorize";
            var unauthorizeInfo = new
            {
                UserName = userName,
                RoleName = "Admin"
            };
            var reqBody = JsonConvert.SerializeObject(unauthorizeInfo);
            await _httpRequest.RequestAsync(HttpMethod.Post, url, null, reqBody);
        }
    }
}
