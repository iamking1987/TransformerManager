﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using TransformerManager.Shared.Helper.HttpParams;

namespace TransformerManager.WPF.Common.Http.Service
{
    public class OperationLogService : IOperationLogService
    {
        private readonly HttpRequest _httpRequest;
        public OperationLogService(HttpRequest httpRequest)
        {
            _httpRequest = httpRequest;
        }

        public async Task<ResponseParams> GetOperationLogsAsync(
            string userName, 
            DateTime operateTimeStart, 
            DateTime operateTimeEnd, 
            int pageIndex, 
            int pageSize)
        {
            string url = $"OperationLog/Query?userName={userName}" +
                $"&operateTimeStart={operateTimeStart}&operateTimeEnd={operateTimeEnd}" +
                $"&pageNumber={pageIndex}&pageSize={pageSize}";
            return await _httpRequest.RequestAsync(HttpMethod.Get, url, null);
        }

        public async Task<string> GetRequestBodyByIdAsync(int id)
        {
            string url = $"OperationLog/RequestBody/{id}";
            var responseParams = await _httpRequest.RequestAsync(HttpMethod.Get, url, null);
            if(responseParams != null)
            {
                return responseParams.Result.ToString();
            }
            return null;
        }
    }
}
