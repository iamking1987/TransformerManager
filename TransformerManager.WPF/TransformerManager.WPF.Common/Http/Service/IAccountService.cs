﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TransformerManager.Shared.Helper.HttpParams;

namespace TransformerManager.WPF.Common.Http.Service
{
    public interface IAccountService
    {
        Task<TokenParams> LoginAsync(LoginParams loginParams);
        Task<ResponseParams> RegisterAsync(RegisterParams registerParams);

    }
}
