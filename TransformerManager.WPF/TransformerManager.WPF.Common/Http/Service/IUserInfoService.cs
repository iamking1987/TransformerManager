﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using TransformerManager.Shared.Helper.HttpParams;

namespace TransformerManager.WPF.Common.Http.Service
{
    public interface IUserInfoService
    {
        Task ModifyPasswordAsync(ModifyPasswordParams modifyPasswordParams);
        Task<UserBasicParams> ModifyUserInfoAsync(UserBasicParams userBasicParams);
        Task<UserBasicParams> GetUserInfoByUserNameAsync(string userName);
        Task<UserBasicParams> GetUserInfoByIdAsync(string id);
        Task<ResponseParams> GetUsersAsync(string keywords, bool onlyLeadEngineer, int pageIndex, int pageSize);

        Task ResetUserPasswordAsync(string userName);
        Task AuthorizeRole(string userName);
        Task UnauthorizeRole(string userName);
    }
}
