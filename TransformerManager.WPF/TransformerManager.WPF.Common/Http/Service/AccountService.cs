﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TransformerManager.Shared.Helper.HttpParams;

namespace TransformerManager.WPF.Common.Http.Service
{
    public class AccountService : IAccountService
    {
        private readonly HttpRequest _httpRequest;
        public AccountService(HttpRequest httpRequest)
        {
            _httpRequest = httpRequest;
        }

        public async Task<TokenParams> LoginAsync(LoginParams loginParams)
        {
            string url = "Authenticate/login";
            string loginInfo = JsonConvert.SerializeObject(loginParams);
            var responseParams = await _httpRequest.RequestAsync(HttpMethod.Post, url, null, loginInfo);

            if (responseParams != null)
            {
                return JsonConvert.DeserializeObject<TokenParams>(responseParams.Result.ToString());
            }
            return null;
        }

        public async Task<ResponseParams> RegisterAsync(RegisterParams registerParams)
        {
            string url = "Authenticate/register";
            string registerInfo = JsonConvert.SerializeObject(registerParams);
            return await _httpRequest.RequestAsync(HttpMethod.Post, url, null, registerInfo);
        }
    }
}
