﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TransformerManager.Shared.Helper.HttpParams;

namespace TransformerManager.WPF.Common.Http.Service
{
    public interface IOperationLogService
    {
        Task<ResponseParams> GetOperationLogsAsync(string userName, DateTime operateTimeStart, DateTime operateTimeEnd, int pageIndex, int pageSize);
        Task<string> GetRequestBodyByIdAsync(int id);
    }
}
