﻿using System.Windows.Controls;

namespace TransformerManager.WPF.Common.Views
{
    /// <summary>
    /// Interaction logic for ModifyPwdDialog
    /// </summary>
    public partial class ModifyPwdDialog : UserControl
    {
        public ModifyPwdDialog()
        {
            InitializeComponent();
        }

        private void CheckInputValue(object sender, System.Windows.Input.KeyEventArgs e)
        {
            PasswordBox tbNewPwd = this.FindName("TbNewPwd") as PasswordBox;
            PasswordBox tbConfirmPwd = this.FindName("TbConfirmPwd") as PasswordBox;
            Button btn = this.FindName("BtnYes") as Button;
            TextBlock textBlock = this.FindName("TbErrorInfo") as TextBlock;

            if (string.IsNullOrEmpty(tbNewPwd.Password.Trim()))
            {
                btn.IsEnabled = false;
                textBlock.Text = "Please input password";
                return;
            }

            if (string.IsNullOrEmpty(tbConfirmPwd.Password.Trim()))
            {
                btn.IsEnabled = false;
                textBlock.Text = "Please confirm password";
                return;
            }

            if (!tbNewPwd.Password.Equals(tbConfirmPwd.Password))
            {
                btn.IsEnabled = false;
                textBlock.Text = "Please confirm password";
                return;
            }

            textBlock.Text = "";
            btn.IsEnabled = true;
        }
    }
}
