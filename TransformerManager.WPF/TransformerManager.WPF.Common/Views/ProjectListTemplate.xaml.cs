﻿using Prism.Regions;
using System.Windows.Controls;
using System.Windows.Input;
using TransformerManager.Shared.Dto.Transformer;

namespace TransformerManager.WPF.Common.Views
{
    /// <summary>
    /// Interaction logic for ProjectListTemplate
    /// </summary>
    public partial class ProjectListTemplate : UserControl
    {
        private readonly IRegionManager _regionManager;

        public ProjectListTemplate(IRegionManager regionManager)
        {
            InitializeComponent();

            _regionManager = regionManager;
        }

        private void OnDirectToDetailClick(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton != MouseButton.Left)
            {
                return;
            }

            DataGrid dataGrid = sender as DataGrid;

            if (!(dataGrid.SelectedItem is TransformerDto itemData))
            {
                return;
            }

            NavigationParameters naviParams = new NavigationParameters
            {
                { "TransformerDto", itemData }
            };

            _regionManager.RequestNavigate("ContentRegion", nameof(DetailInfoTemplate), naviParams);
        }
    }
}
