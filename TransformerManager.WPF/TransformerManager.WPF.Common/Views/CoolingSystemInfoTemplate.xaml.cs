﻿using Prism.Common;
using Prism.Events;
using Prism.Regions;
using System.Collections;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using TransformerManager.Shared.Dto.CoolEquComp;
using TransformerManager.WPF.Common.Helper.Events;
using TransformerManager.WPF.Common.ViewModels;

namespace TransformerManager.WPF.Common.Views
{
    /// <summary>
    /// Interaction logic for CoolingSystemInfoTemplate
    /// </summary>
    public partial class CoolingSystemInfoTemplate : UserControl
    {
        private readonly IEventAggregator _eventAggregator;
        public CoolingSystemInfoTemplate(IEventAggregator eventAggregator)
        {
            InitializeComponent();

            _eventAggregator = eventAggregator;

            RegionContext.GetObservableContext(this).PropertyChanged += CoolingSystemInfo_PropertyChanged;

            _eventAggregator.GetEvent<ReloadXmlDataPageEvent>()
                .Subscribe(ReloadXmlData, ThreadOption.PublisherThread, false,
                msg=>msg.Contains(nameof(CoolingSystemInfoTemplate)));
        }

        private void CoolingSystemInfo_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            // sender是名为DetailInfoRegion的控件ContentControl下绑定的BasicInfosDto参数
            var context = (ObservableObject<object>)sender;
            var coolingSysDto = (CoolingSystemDto)context.Value;
            // 赋值给BasicInfosTemplateViewModel 下定义的BasicInfosDto方法
            (DataContext as CoolingSystemInfoTemplateViewModel).CoolingSystemDto = coolingSysDto;
        }

        private async void ReloadXmlData(string value)
        {
            foreach (DictionaryEntry resource in Resources)
            {
                string typeName = resource.Value.GetType().Name;
                if (typeName == "XmlDataProvider")
                {
                    await Task.Factory.StartNew(() =>
                    {
                        ((XmlDataProvider)resource.Value).Refresh();
                    });

                }
            }
        }

    }
}
