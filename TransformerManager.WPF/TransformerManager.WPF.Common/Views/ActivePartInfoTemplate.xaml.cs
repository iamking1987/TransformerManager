﻿using Prism.Common;
using Prism.Events;
using Prism.Regions;
using System.Collections;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using TransformerManager.Shared.Dto.ActivePart;
using TransformerManager.WPF.Common.Helper.Events;
using TransformerManager.WPF.Common.ViewModels;

namespace TransformerManager.WPF.Common.Views
{
    /// <summary>
    /// Interaction logic for ActivePartInfoTemplate
    /// </summary>
    public partial class ActivePartInfoTemplate : UserControl
    {

        private readonly IEventAggregator _eventAggregator;

        public ActivePartInfoTemplate(IEventAggregator eventAggregator)
        {
            InitializeComponent();

            _eventAggregator = eventAggregator;

            RegionContext.GetObservableContext(this).PropertyChanged += ActivePartInfo_PropertyChanged;

            _eventAggregator.GetEvent<ReloadXmlDataPageEvent>()
                .Subscribe(ReloadXmlData, ThreadOption.PublisherThread, false,
                msg => msg.Contains(nameof(ActivePartInfoTemplate)));
        }

        private void ActivePartInfo_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            // sender是名为DetailInfoRegion的控件ContentControl下绑定的TankDto参数
            var context = (ObservableObject<object>)sender;
            var activePartDto = (ActivePartDto)context.Value;
            // 赋值给TankInfoTemplateViewModel 下定义的TankDto方法
            (DataContext as ActivePartInfoTemplateViewModel).ActivePartDto = activePartDto;
        }

        private async void ReloadXmlData(string value)
        {
            foreach (DictionaryEntry resource in Resources)
            {
                string typeName = resource.Value.GetType().Name;
                if (typeName == "XmlDataProvider")
                {
                    await Task.Factory.StartNew(() =>
                    {
                        ((XmlDataProvider)resource.Value).Refresh();
                    });

                }
            }
        }
    }
}
