﻿using Prism.Common;
using Prism.Regions;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using TransformerManager.Shared.Dto.Transformer;
using TransformerManager.WPF.Common.ViewModels;
using hc = HandyControl.Controls;

namespace TransformerManager.WPF.Common.Views
{
    /// <summary>
    /// Interaction logic for ProjectInfoTemplate
    /// </summary>
    public partial class ProjectInfoTemplate : UserControl
    {
        private readonly IRegionManager _regionManager;

        public ProjectInfoTemplate(IRegionManager regionManager)
        {
            InitializeComponent();

            _regionManager = regionManager;

            RegionContext.GetObservableContext(this).PropertyChanged += ProjectInfo_PropertyChanged;
        }

        private void TxtCoolingTypeLostFocus(object sender, RoutedEventArgs e)
        {
            TextBox txt = sender as TextBox;
            var arr = txt.Text.Split("|");
            string coolingType = "";
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i].Length == 4)
                {
                    coolingType += arr[i] + "|";
                }
            }

            if (!string.IsNullOrEmpty(coolingType))
                txt.Text = coolingType[0..^1];
        }

        private void ProjectInfo_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            // sender是名为DetailInfoRegion的控件ContentControl下绑定的TransformerDto参数
            var context = (ObservableObject<object>)sender;
            var transformerDto = (TransformerDto)context.Value;
            // 赋值给ProjectInfoTemplateViewModel 下定义的TransformerDto方法
            (DataContext as ProjectInfoTemplateViewModel).TransformerDto = transformerDto;
        }

        private void BasicInfosItem_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            _regionManager.RequestNavigate("BasicInfosRegion", nameof(BasicInfosTemplate));
        }
        private void TankItem_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            _regionManager.RequestNavigate("TankInfoRegion", nameof(TankInfoTemplate));
        }
        private void ConservatorItem_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            _regionManager.RequestNavigate("ConservatorInfoRegion", nameof(ConservatorInfoTemplate));
        }
        private void CoolingSystemItem_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            _regionManager.RequestNavigate("CoolingSystemInfoRegion", nameof(CoolingSystemInfoTemplate));
        }
        private void AccessoriesItem_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            _regionManager.RequestNavigate("AccessoriesInfoRegion", nameof(AccessoriesInfoTemplate));
        }
        private void LeadItem_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            _regionManager.RequestNavigate("LeadInfoRegion", nameof(LeadInfoTemplate));
        }
        private void WindingItem_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            _regionManager.RequestNavigate("WindingInfoRegion", nameof(WindingInfoTemplate));
        }
        private void ActivePartItem_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            _regionManager.RequestNavigate("ActivePartInfoRegion", nameof(ActivePartInfoTemplate));
        }
    }
}
