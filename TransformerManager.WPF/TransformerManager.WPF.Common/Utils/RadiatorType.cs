﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransformerManager.WPF.Common.Utils
{
    public class RadiatorType
    {
        public double RadHeight { set; get; }
        public double RadNumber { set; get; }
        public double RadWidth { set; get; }
    }
}
