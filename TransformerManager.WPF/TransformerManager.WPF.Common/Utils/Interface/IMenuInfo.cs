﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransformerManager.WPF.Common.Utils.Interface
{
    public interface IMenuInfo
    {
        /// <summary>
        /// Icon字体代码
        /// </summary>
        public string IconFontText { get; }

        /// <summary>
        /// 模块权限, 本项目中管理员可见则可设置为"Admin", 否则为空
        /// </summary>
        public string ModuleAuth { get; }

        /// <summary>
        /// 导航栏字体颜色
        /// </summary>
        public string IconFontColor { get; }

        /// <summary>
        /// 模块的视图名称
        /// </summary>
        public string ViewName { get; }
    }
}
