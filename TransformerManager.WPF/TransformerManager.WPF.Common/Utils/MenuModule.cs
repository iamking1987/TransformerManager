﻿using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace TransformerManager.WPF.Common.Utils
{
    public class MenuModule
    {
        public string ModuleName { get; set; }

        public string IconFontText { get; set; }

        public string IconFontColor { get; set; }

        public DelegateCommand NavigateCommand { get; set; }

    }
}
