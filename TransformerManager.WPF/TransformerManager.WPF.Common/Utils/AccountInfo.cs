﻿using System.Collections.Generic;

namespace TransformerManager.WPF.Common.Utils
{
    public static class AccountInfo
    {
        public static string JwtToken { set; get; }

        public static string UserId { set; get; }

        public static string UserName { set; get; }

        public static string RealNameEn { set; get; }

        public static string RealNameCn { set; get; }

        public static string Department { set; get; }

        public static bool IsLeadEngineer { set; get; }

        public static IList<string> Roles { set; get; }
    }
}
