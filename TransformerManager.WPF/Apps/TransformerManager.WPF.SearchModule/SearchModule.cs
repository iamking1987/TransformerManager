﻿using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;
using TransformerManager.WPF.Common.Utils.Interface;
using TransformerManager.WPF.SearchModule.ViewModels;
using TransformerManager.WPF.SearchModule.Views;

namespace TransformerManager.WPF.SearchModule
{
    [Module(ModuleName = "搜索")]
    public class SearchModule : IModule, IMenuInfo
    {
        public string IconFontText { get => "&#xeba3;"; }
        public string ModuleAuth { get => string.Empty; }
        public string IconFontColor { get => "#a3c7e9"; }
        public string ViewName { get => nameof(SearchProject); }

        public void OnInitialized(IContainerProvider containerProvider)
        {
            //通过注册RegionManager，添加ContactView
            //var regionManager = containerProvider.Resolve<IRegionManager>();
            //通过ContentRegion管理导航默认初始页面ContactView
            //var contentRegion = regionManager.Regions["ContentRegion"];
            //contentRegion.RequestNavigate(nameof(SearchProject));
            //regionManager.RegisterViewWithRegion("ContentRegion", typeof(SearchProject));
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<SearchProject, SearchProjectViewModel>();
        }
    }
}