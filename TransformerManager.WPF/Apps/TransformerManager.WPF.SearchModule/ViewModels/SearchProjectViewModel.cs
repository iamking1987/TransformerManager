﻿using AutoMapper;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using TransformerManager.Shared.Dto.Transformer;
using TransformerManager.Shared.ResourceParameters;
using TransformerManager.WPF.Common.Helper;
using TransformerManager.WPF.Common.Views;
using TransformerManager.WPF.SearchModule.Views;

namespace TransformerManager.WPF.SearchModule.ViewModels
{
    public class SearchProjectViewModel : BindableBase, INavigationAware
    {
        private readonly IRegionManager _regionManager;
        private readonly IMapper _mapper;

        private SearchProject _searchProject;
        public SearchProjectViewModel(
            IMapper mapper,
            IRegionManager regionManager)
        {
            _regionManager = regionManager;
            _mapper = mapper;

            InitAllCmd();
        }

        #region Command

        public DelegateCommand SearchCmd { private set; get; }


        #endregion


        #region 属性的定义

        private Visibility _isLoadingVisible = Visibility.Collapsed;
        public Visibility IsLoadingVisible
        {
            set
            {
                _isLoadingVisible = value;

                RaisePropertyChanged();
            }
            get
            {
                return _isLoadingVisible;
            }
        }

        private bool _isSearchItemEnable = true;
        public bool IsSearchItemEnable
        {
            set
            {
                _isSearchItemEnable = value;

                RaisePropertyChanged();
            }
            get
            {
                return _isSearchItemEnable;
            }
        }

        private bool _isChkBasicSel = false;
        public bool IsChkBasicSel

        {
            set
            {
                _isChkBasicSel = value;

                RaisePropertyChanged();
            }
            get
            {
                return _isChkBasicSel;
            }
        }

        private bool _isChkTankSel = false;
        public bool IsChkTankSel

        {
            set
            {
                _isChkTankSel = value;

                RaisePropertyChanged();
            }
            get
            {
                return _isChkTankSel;
            }
        }

        private bool _isChkConVSel = false;
        public bool IsChkConVSel

        {
            set
            {
                _isChkConVSel = value;

                RaisePropertyChanged();
            }
            get
            {
                return _isChkConVSel;
            }
        }

        private bool _isChkTCSel = false;
        public bool IsChkTCSel

        {
            set
            {
                _isChkTCSel = value;

                RaisePropertyChanged();
            }
            get
            {
                return _isChkTCSel;
            }
        }

        private bool _isChkDomeSel = false;
        public bool IsChkDomeSel

        {
            set
            {
                _isChkDomeSel = value;

                RaisePropertyChanged();
            }
            get
            {
                return _isChkDomeSel;
            }
        }

        private bool _isChkBushingSel = false;
        public bool IsChkBushingSel

        {
            set
            {
                _isChkBushingSel = value;

                RaisePropertyChanged();
            }
            get
            {
                return _isChkBushingSel;
            }
        }

        private bool _isChkCoolingSysSel = false;
        public bool IsChkCoolingSysSel

        {
            set
            {
                _isChkCoolingSysSel = value;

                RaisePropertyChanged();
            }
            get
            {
                return _isChkCoolingSysSel;
            }
        }

        private bool _isChkAccSel = false;
        public bool IsChkAccSel

        {
            set
            {
                _isChkAccSel = value;

                RaisePropertyChanged();
            }
            get
            {
                return _isChkAccSel;
            }
        }

        private bool _isChkAccEquSel = false;
        public bool IsChkAccEquSel

        {
            set
            {
                _isChkAccEquSel = value;

                RaisePropertyChanged();
            }
            get
            {
                return _isChkAccEquSel;
            }
        }

        private bool _isChkLeadSel = false;
        public bool IsChkLeadSel

        {
            set
            {
                _isChkLeadSel = value;

                RaisePropertyChanged();
            }
            get
            {
                return _isChkLeadSel;
            }
        }

        private bool _isChkLeadConnSel = false;
        public bool IsChkLeadConnSel

        {
            set
            {
                _isChkLeadConnSel = value;

                RaisePropertyChanged();
            }
            get
            {
                return _isChkLeadConnSel;
            }
        }

        private bool _isChkHVRWindingSel = false;
        public bool IsChkHVRWindingSel

        {
            set
            {
                _isChkHVRWindingSel = value;

                RaisePropertyChanged();
            }
            get
            {
                return _isChkHVRWindingSel;
            }
        }

        private bool _isChkDiscWindingSel = false;
        public bool IsChkDiscWindingSel

        {
            set
            {
                _isChkDiscWindingSel = value;

                RaisePropertyChanged();
            }
            get
            {
                return _isChkDiscWindingSel;
            }
        }

        private bool _isChkLayerWindingSel = false;
        public bool IsChkLayerWindingSel

        {
            set
            {
                _isChkLayerWindingSel = value;

                RaisePropertyChanged();
            }
            get
            {
                return _isChkLayerWindingSel;
            }
        }

        private bool _isChkActivePartSel = false;
        public bool IsChkActivePartSel

        {
            set
            {
                _isChkActivePartSel = value;

                RaisePropertyChanged();
            }
            get
            {
                return _isChkActivePartSel;
            }
        }

        private ObservableCollection<TransformerDto> _showTrDataList
            = new ObservableCollection<TransformerDto>();
        public ObservableCollection<TransformerDto> ShowTrDataList
        {
            set
            {
                _showTrDataList = value;

                RaisePropertyChanged();
            }
            get
            {
                return _showTrDataList;
            }
        }

        private TransformerResourceParameters _trResourceParams
            = new TransformerResourceParameters()
            {
                BasicInfos = new BasicInfosResourceParameters(),
                TapChanger = new TapChangerResourceParameters(),
                Dome = new DomeResourceParameters(),
                Bushing = new BushingResourceParameters(),
                Tank = new TankResourceParameters(),
                Conservator = new ConservatorResourceParameters(),
                CoolingSystem = new CoolingSystemResourceParameters(),
                Accessories = new AccessoriesResourceParameters(),
                AccesstoryEquipment = new AccessoryEquipmentResourceParameters(),
                Lead = new LeadResourceParameters(),
                LeadConnector = new LeadConnectorResourceParameters(),
                Winding = new WindingResourceParameters()
                {
                    HVRWinding = new HVRWindingResourceParameters(),
                    DiscWinding = new DiscWindingResourceParameters(),
                    LayerWinding = new LayerWindingResourceParameters()
                },
                ActivePart = new ActivePartResourceParameters()
            };
        public TransformerResourceParameters TrResourceParams
        {
            set
            {
                _trResourceParams = value;

                RaisePropertyChanged();
            }
            get
            {
                return _trResourceParams;
            }
        }

        #endregion


        #region 函数的定义

        private void InitAllCmd()
        {
            SearchCmd = new DelegateCommand(() =>
            {
                IsLoadingVisible = Visibility.Visible;
                IsSearchItemEnable = false;

                SearchTransformerInfo();

                IsLoadingVisible = Visibility.Collapsed;
                IsSearchItemEnable = true;
            });
        }

        private void SearchTransformerInfo()
        {
            var trR = new TransformerResourceParameters()
            {
                BasicInfos = TrResourceParams.BasicInfos,
                TapChanger = TrResourceParams.TapChanger,
                Dome = TrResourceParams.Dome,
                Bushing = TrResourceParams.Bushing,
                Tank = TrResourceParams.Tank,
                Conservator = TrResourceParams.Conservator,
                CoolingSystem = TrResourceParams.CoolingSystem,
                Accessories = TrResourceParams.Accessories,
                AccesstoryEquipment = TrResourceParams.AccesstoryEquipment,
                Lead = TrResourceParams.Lead,
                LeadConnector = TrResourceParams.LeadConnector,
                Winding = TrResourceParams.Winding,
                ActivePart = TrResourceParams.ActivePart
            };

            if (!IsChkBasicSel)
            {
                trR.BasicInfos = null;
            }

            if (!IsChkTankSel)
            {
                trR.Tank = null;
            }

            if (!IsChkConVSel)
            {
                trR.Conservator = null;
            }

            if (!IsChkTCSel)
            {
                trR.TapChanger = null;
            }

            if (!IsChkDomeSel)
            {
                trR.Dome = null;
            }

            if (!IsChkBushingSel)
            {
                trR.Bushing = null;
            }

            if (!IsChkCoolingSysSel)
            {
                trR.CoolingSystem = null;
            }

            if (!IsChkAccSel)
            {
                trR.Accessories = null;
            }

            if (!IsChkAccEquSel)
            {
                trR.AccesstoryEquipment = null;
            }

            if (!IsChkLeadSel)
            {
                trR.Lead = null;
            }

            if (!IsChkLeadConnSel)
            {
                trR.LeadConnector = null;
            }

            if (!IsChkHVRWindingSel)
            {
                trR.Winding.HVRWinding = null;
            }

            if (!IsChkDiscWindingSel)
            {
                trR.Winding.DiscWinding = null;
            }

            if (!IsChkLayerWindingSel)
            {
                trR.Winding.LayerWinding = null;
            }

            if (!IsChkActivePartSel)
            {
                trR.ActivePart = null;
            }

            NavigationParameters naviParams = new NavigationParameters
            {
                { "TransformerResourceParameters", trR }
            };

            _regionManager.RequestNavigate("SearchProjectRegion", nameof(ProjectListTemplate), naviParams);
        }

        #endregion

        #region 接口的实现
        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            if (_searchProject == null)
            {
                var regionName = navigationContext.NavigationService.Region.Name;

                _searchProject = _regionManager.Regions[regionName].ActiveViews
                    .Where(v => v.GetType() == typeof(SearchProject))
                    .FirstOrDefault() as SearchProject;
            }
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            CustomiseRegionManager.RemoveRegions(navigationContext);
        }

        #endregion
    }
}
