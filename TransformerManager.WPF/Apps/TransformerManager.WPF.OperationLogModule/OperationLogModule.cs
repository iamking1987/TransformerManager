﻿using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;
using TransformerManager.WPF.Common.Utils.Interface;
using TransformerManager.WPF.OperationLogModule.ViewModels;
using TransformerManager.WPF.OperationLogModule.Views;

namespace TransformerManager.WPF.OperationLogModule
{
    [Module(ModuleName = "日志")]
    public class OperationLogModule : IModule, IMenuInfo
    {
        public string IconFontText{ get => "&#xebad;"; }

        public string ModuleAuth { get => "Admin"; }

        public string IconFontColor { get => "#FFA500"; }

        public string ViewName { get => nameof(OperationLog); }

        public void OnInitialized(IContainerProvider containerProvider)
        {
            //通过注册RegionManager，添加ContactView
            //var regionManager = containerProvider.Resolve<IRegionManager>();
            //通过ContentRegion管理导航默认初始页面ContactView
            //regionManager.RegisterViewWithRegion("ContentRegion", typeof(OperationLog));
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<OperationLog, OperationLogViewModel>();
        }
    }
}