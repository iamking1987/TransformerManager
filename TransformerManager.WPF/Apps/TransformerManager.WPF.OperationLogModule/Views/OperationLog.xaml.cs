﻿using System;
using System.Windows.Controls;
using TransformerManager.WPF.OperationLogModule.ViewModels;
using hc = HandyControl.Controls;

namespace TransformerManager.WPF.OperationLogModule.Views
{
    /// <summary>
    /// Interaction logic for OperationLog
    /// </summary>
    public partial class OperationLog : UserControl
    {
        public OperationLog()
        {
            InitializeComponent();
        }

        private async void SearchBar_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            hc.SearchBar searchBar = sender as hc.SearchBar;
            var opLogModuleVM = this.DataContext as OperationLogViewModel;
            if (e.Key == System.Windows.Input.Key.Escape &&
                !string.IsNullOrEmpty(searchBar.Text.Trim()))
            {
                searchBar.Text = string.Empty;
                await opLogModuleVM.RetrieveDataGridSource(string.Empty, DateTime.Today.AddDays(-15), DateTime.Today.AddHours(12) , 1, 30);
            }
        }
    }
}
