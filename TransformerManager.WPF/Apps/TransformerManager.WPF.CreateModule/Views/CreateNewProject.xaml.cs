﻿using System.Windows;
using System.Windows.Controls;
using hc = HandyControl.Controls;

namespace TransformerManager.WPF.CreateModule.Views
{
    /// <summary>
    /// Interaction logic for CreateNewProject
    /// </summary>
    public partial class CreateNewProject : UserControl
    {
        public CreateNewProject()
        {
            InitializeComponent();
        }

        public void CheckClickedBtnsStatusUpdate()
        {
            Grid content = this.FindName("CreateNewProjectContent") as Grid;
            content.Visibility = Visibility.Visible;

            Button btnCheck = this.FindName("BtnCheckProject") as Button;
            btnCheck.IsEnabled = false;

            Button btnDone = this.FindName("BtnDone") as Button;
            btnDone.IsEnabled = true;

            TextBox tbProjNumber = this.FindName("TbProjNumber") as TextBox;
            tbProjNumber.IsEnabled = false;
        }

        public void InitViewCtrllerStatus()
        {
            Grid content = this.FindName("CreateNewProjectContent") as Grid;
            content.Visibility = Visibility.Hidden;

            TextBox tbProjNumber = this.FindName("TbProjNumber") as TextBox;
            tbProjNumber.IsEnabled = true;

            Button btnCheck = this.FindName("BtnCheckProject") as Button;
            btnCheck.IsEnabled = true;

            Button btnDone = this.FindName("BtnDone") as Button;
            btnDone.IsEnabled = false;
        }

    }
}
