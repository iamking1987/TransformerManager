﻿using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;
using TransformerManager.WPF.Common.Utils.Interface;
using TransformerManager.WPF.CreateModule.ViewModels;
using TransformerManager.WPF.CreateModule.Views;

namespace TransformerManager.WPF.CreateModule
{
    [Module(ModuleName = "新建")]
    public class CreateModule : IModule, IMenuInfo
    {
        public string IconFontText { get => "&#xebae;"; }
        public string ModuleAuth { get => string.Empty; }
        public string IconFontColor { get => "#f4ea2a"; }
        public string ViewName { get => nameof(CreateNewProject); }

        public void OnInitialized(IContainerProvider containerProvider)
        {
            //通过注册RegionManager，添加ContactView
            //var regionManager = containerProvider.Resolve<IRegionManager>();
            //通过ContentRegion管理导航默认初始页面ContactView
            //var contentRegion = regionManager.Regions["ContentRegion"];
            //regionManager.RegisterViewWithRegion("ContentRegion", typeof(CreateNewProject));
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<CreateNewProject, CreateNewProjectViewModel>();
        }
    }
}