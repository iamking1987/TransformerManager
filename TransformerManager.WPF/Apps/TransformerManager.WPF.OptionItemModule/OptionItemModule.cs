﻿using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;
using TransformerManager.WPF.Common.Utils.Interface;
using TransformerManager.WPF.OptionItemModule.ViewModels;
using TransformerManager.WPF.OptionItemModule.Views;

namespace TransformerManager.WPF.OptionItemModule
{
    [Module(ModuleName = "条目")]
    public class OptionItemModule : IModule, IMenuInfo
    {
        public string IconFontText { get => "&#xebb1;"; }
        public string ModuleAuth { get => string.Empty; }
        public string IconFontColor { get => "#00ff00"; }
        public string ViewName { get => nameof(OptionItem); }
        public void OnInitialized(IContainerProvider containerProvider)
        {
            //通过注册RegionManager，添加ContactView
            //var regionManager = containerProvider.Resolve<IRegionManager>();
            //通过ContentRegion管理导航默认初始页面ContactView
            //regionManager.RegisterViewWithRegion("ContentRegion", typeof(OptionItem));
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<OptionItem, OptionItemViewModel>();
            containerRegistry.RegisterDialog<OptionItemDialog, OptionItemDialogViewModel>("OptionItemDialog");
        }
    }
}