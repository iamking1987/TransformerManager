﻿using System.Windows.Controls;
using TransformerManager.WPF.UsersModule.ViewModels;
using hc = HandyControl.Controls;
namespace TransformerManager.WPF.UsersModule.Views
{
    /// <summary>
    /// Interaction logic for Users
    /// </summary>
    public partial class Users : UserControl
    {
        public Users()
        {
            InitializeComponent();
        }

        private async void SearchBar_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            hc.SearchBar searchBar = sender as hc.SearchBar;
            var usersModuleVM = this.DataContext as UsersViewModel;
            if (e.Key == System.Windows.Input.Key.Escape &&
                !string.IsNullOrEmpty(searchBar.Text.Trim()))
            {
                searchBar.Text = string.Empty;
                await usersModuleVM.RetrieveUserDataList(
                    usersModuleVM.PageIndex, usersModuleVM.DataCountPerPage);
            }
        }
    }
}
