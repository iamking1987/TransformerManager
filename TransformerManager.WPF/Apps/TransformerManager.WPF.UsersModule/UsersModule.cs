﻿using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;
using TransformerManager.WPF.Common.Utils.Interface;
using TransformerManager.WPF.UsersModule.ViewModels;
using TransformerManager.WPF.UsersModule.Views;

namespace TransformerManager.WPF.UsersModule
{
    [Module(ModuleName = "用户")]
    public class UsersModule : IModule, IMenuInfo
    {
        public string IconFontText { get => "&#xeba6;"; }
        public string ModuleAuth { get => "Admin"; }
        public string IconFontColor { get => "#ff7272"; }
        public string ViewName { get => nameof(Users); }

        public void OnInitialized(IContainerProvider containerProvider)
        {
            //通过注册RegionManager，添加ContactView
            //var regionManager = containerProvider.Resolve<IRegionManager>();
            //通过ContentRegion管理导航默认初始页面ContactView
            //var contentRegion = regionManager.Regions["ContentRegion"];
            //contentRegion.RequestNavigate(nameof(Users));
            //regionManager.RegisterViewWithRegion("ContentRegion", typeof(Users));
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<Users, UsersViewModel>();
        }
    }
}