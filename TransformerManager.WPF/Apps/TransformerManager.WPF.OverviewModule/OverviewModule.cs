﻿using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;
using TransformerManager.WPF.Common.Utils.Interface;
using TransformerManager.WPF.OverviewModule.ViewModels;
using TransformerManager.WPF.OverviewModule.Views;

namespace TransformerManager.WPF.OverviewModule
{
    [Module(ModuleName = "总览")]
    public class OverviewModule : IModule, IMenuInfo
    {
        public string IconFontText { get => "&#xeba4;"; }
        public string ModuleAuth { get => string.Empty; }
        public string IconFontColor { get => "#5de1c8"; }
        public string ViewName { get => nameof(Overview); }

        public void OnInitialized(IContainerProvider containerProvider)
        {
            //通过注册RegionManager，添加ContactView
            //var regionManager = containerProvider.Resolve<IRegionManager>();
            //通过ContentRegion管理导航默认初始页面ContactView
            //var contentRegion = regionManager.Regions["ContentRegion"];
            //contentRegion.RequestNavigate(nameof(Overview));
            //regionManager.RegisterViewWithRegion("ContentRegion", typeof(Overview));
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<Overview, OverviewViewModel>();
        }
    }
}