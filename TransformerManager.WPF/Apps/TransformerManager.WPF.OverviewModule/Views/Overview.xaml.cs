﻿using System.Windows.Controls;

namespace TransformerManager.WPF.OverviewModule.Views
{
    /// <summary>
    /// Interaction logic for Overview
    /// </summary>
    public partial class Overview : UserControl
    {
        public Overview()
        {
            InitializeComponent();
        }
    }
}
