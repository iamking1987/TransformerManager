﻿using AutoMapper;
using LiveCharts;
using LiveCharts.Wpf;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using TransformerManager.Shared.ResourceParameters;
using TransformerManager.WPF.Common.Http;
using TransformerManager.WPF.Common.Http.Service;
using TransformerManager.WPF.Common.Views;

namespace TransformerManager.WPF.OverviewModule.ViewModels
{
    public class OverviewViewModel : BindableBase, INavigationAware
    {
        private readonly IRegionManager _regionManager;
        private readonly ITransformerService _transformerService;
        private readonly IMapper _mapper;
        public OverviewViewModel(
            IMapper mapper,
            ITransformerService transformerService,
            IRegionManager regionManager)
        {
            _regionManager = regionManager;
            _transformerService = transformerService;

            _mapper = mapper;

            //GetRegionDataOfTransformer();

            //ShowTransformersInfo();

            InitAllCmd();
        }

        #region Command

        public DelegateCommand ApplyCmd { set; get; }

        #endregion

        #region 方法的定义

        private SeriesCollection _seriesCollection
            = new SeriesCollection();
        public SeriesCollection SeriesCollection
        {
            set
            {
                _seriesCollection = value;

                RaisePropertyChanged();
            }
            get
            {
                return _seriesCollection;
            }
        }

        private int _yAxisStep;
        public int YAxisStep
        {
            set
            {
                _yAxisStep = value;

                RaisePropertyChanged();
            }
            get
            {
                return _yAxisStep == 0 ? 1 : _yAxisStep;
            }
        }

        private DateTime _from = DateTime.Now.AddYears(-1);
        public DateTime From
        {
            set
            {
                _from = value;

                RaisePropertyChanged();
            }
            get
            {
                return _from;
            }
        }

        private DateTime _end = DateTime.Now;
        public DateTime End
        {
            set
            {
                _end = value;

                RaisePropertyChanged();
            }
            get
            {
                return _end;
            }
        }

        #endregion

        #region 函数的定义

        private void InitAllCmd()
        {
            ApplyCmd = new DelegateCommand(async () =>
            {
                await GetRegionDataOfTransformerAsync(From, End);
            });
        }

        private void ShowTransformersInfo()
        {
            TransformerResourceParameters trResourceParams = new TransformerResourceParameters();

            NavigationParameters naviParams = new NavigationParameters
            {
                { "TransformerResourceParameters", trResourceParams }
            };

            _regionManager.RequestNavigate("OverviewRegion", nameof(ProjectListTemplate), naviParams);
        }

        private async Task GetRegionDataOfTransformerAsync(DateTime from, DateTime end)
        {

            var domesticNum = await _transformerService.GetTransformersNumByRegionAsync("Domestic", from, end);

            var exportNum = await _transformerService.GetTransformersNumByRegionAsync("Export", from, end);

            decimal yAxisMaxValue = domesticNum > exportNum ? domesticNum : exportNum;

            YAxisStep = decimal.ToInt32(Math.Floor(yAxisMaxValue / 4));

            SeriesCollection = new SeriesCollection {
                new ColumnSeries
                {
                    Values = new ChartValues<int>{ domesticNum, exportNum},
                    DataLabels = true,
                }
            };
        }

        #endregion

        #region 接口实现的函数

        public async void OnNavigatedTo(NavigationContext navigationContext)
        {
            ShowTransformersInfo();

            await GetRegionDataOfTransformerAsync(_from, _end);
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
        }

        #endregion

    }
}
