﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using TransformerManager.WPF.Common.Views;

namespace TransformerManager.WPF.Main.ViewModels
{
    public class ReloadXmlDataOptionsDialogViewModel : BindableBase, IDialogAware
    {
        public ReloadXmlDataOptionsDialogViewModel()
        {

            InitCmd();
        }

        #region Command的定义

        public DelegateCommand YesCmd { set; get; }

        public DelegateCommand NoCmd { set; get; }

        #endregion


        #region 方法的定义
        public string Title => "刷新Xml数据";

        public bool IsReloadBasicTabXml { get; set; } = false;
        public bool IsReloadTankTabXml { get; set; } = false;
        public bool IsReloadConvTabXml { get; set; } = false;
        public bool IsReloadCoolingSysTabXml { get; set; } = false;
        public bool IsReloadAccTabXml { get; set; } = false;
        public bool IsReloadLeadTabXml { get; set; } = false;
        public bool IsReloadWindingTabXml { get; set; } = false;
        public bool IsReloadACPTabXml { get; set; } = false;

        #endregion


        #region 事件的定义

        public event Action<IDialogResult> RequestClose;

        #endregion


        #region 函数的定义

        private void InitCmd()
        {
            YesCmd = new DelegateCommand(() =>
            {
                OnDialogClosed();
            });

            NoCmd = new DelegateCommand(() =>
            {
                RequestClose?.Invoke(new DialogResult(ButtonResult.No));
            });
        }

        #endregion


        #region 接口的实现

        public bool CanCloseDialog()
        {
            return true;
        }

        public void OnDialogClosed()
        {
            List<string> reloadXmlTabNames = new List<string>();

            if (IsReloadBasicTabXml) reloadXmlTabNames.Add(nameof(BasicInfosTemplate));
            if (IsReloadTankTabXml) reloadXmlTabNames.Add(nameof(TankInfoTemplate));
            if (IsReloadConvTabXml) reloadXmlTabNames.Add(nameof(ConservatorInfoTemplate));
            if (IsReloadCoolingSysTabXml) reloadXmlTabNames.Add(nameof(CoolingSystemInfoTemplate));
            if (IsReloadAccTabXml) reloadXmlTabNames.Add(nameof(AccessoriesInfoTemplate));
            if (IsReloadLeadTabXml) reloadXmlTabNames.Add(nameof(LeadInfoTemplate));
            if (IsReloadWindingTabXml) reloadXmlTabNames.Add(nameof(WindingInfoTemplate));
            if (IsReloadACPTabXml) reloadXmlTabNames.Add(nameof(ActivePartInfoTemplate));

            var callbackParams = new DialogParameters()
            {
                { "ReloadXmlTabNames", reloadXmlTabNames }
            };

            RequestClose?.Invoke(new DialogResult(ButtonResult.OK, callbackParams));
        }

        public void OnDialogOpened(IDialogParameters parameters)
        {

        }

        #endregion
    }
}
