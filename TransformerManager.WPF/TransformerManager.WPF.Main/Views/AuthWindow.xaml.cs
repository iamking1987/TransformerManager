﻿using Prism.Events;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using TransformerManager.WPF.Common.Helper.Events;
using hc = HandyControl.Controls;


namespace TransformerManager.WPF.Main.Views
{
    /// <summary>
    /// Interaction logic for AuthWindow.xaml
    /// </summary>
    public partial class AuthWindow : Window
    {

        private readonly IEventAggregator _eventAggregator;

        public AuthWindow(IEventAggregator eventAggregator)
        {
            InitializeComponent();

            this.MouseDown += (sender, e) => {
                if (e.LeftButton == MouseButtonState.Pressed)
                {
                    this.DragMove();
                }
            };

            _eventAggregator = eventAggregator;

            _eventAggregator.GetEvent<LoginEvent>().Subscribe(CloseCurrentWindow);

        }

        private void CloseCurrentWindow(bool? result)
        {
            // DialogResult 参数得到赋值,则会关闭当前窗口
            this.DialogResult = result;
        }

        private void CheckInputValue(object sender, System.Windows.Input.KeyEventArgs e)
        {
            PasswordBox firstPasswordBox = this.FindName("FirstPasswordBox") as PasswordBox;
            PasswordBox secondPasswordBox = this.FindName("SecondPasswordBox") as PasswordBox;
            hc.TextBox tbAccount = this.FindName("TbUserName") as hc.TextBox;
            hc.TextBox tbRealNameEn = this.FindName("TbRealNameEn") as hc.TextBox;
            TextBlock textBlock = this.FindName("TbErrorInfo") as TextBlock;
            Button btn = this.FindName("BtnRegister") as Button;

            if (string.IsNullOrEmpty(firstPasswordBox.Password.Trim()))
            {
                btn.IsEnabled = false;
                textBlock.Text = "Please input password";
                return;
            }

            if (string.IsNullOrEmpty(secondPasswordBox.Password.Trim()))
            {
                btn.IsEnabled = false;
                textBlock.Text = "Please confirm password";
                return;
            }

            if (!firstPasswordBox.Password.Equals(secondPasswordBox.Password))
            {
                btn.IsEnabled = false;
                textBlock.Text = "Please confirm password";
                return;
            }

            if (string.IsNullOrEmpty(tbAccount.Text.Trim()))
            {
                btn.IsEnabled = false;
                tbAccount.IsError = true;
                tbAccount.ErrorStr = "Please input user name!";
                return;
            }

            if (string.IsNullOrEmpty(tbRealNameEn.Text.Trim()))
            {
                btn.IsEnabled = false;
                tbRealNameEn.IsError = true;
                tbRealNameEn.ErrorStr = "Please input your name!";
                return;
            }

            textBlock.Text = "";
            btn.IsEnabled = true;
        }

    }
}
