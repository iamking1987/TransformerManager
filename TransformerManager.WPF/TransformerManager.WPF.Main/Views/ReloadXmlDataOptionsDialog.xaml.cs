﻿using System.Windows.Controls;

namespace TransformerManager.WPF.Main.Views
{
    /// <summary>
    /// Interaction logic for ReloadXmlDataOptionsDialog
    /// </summary>
    public partial class ReloadXmlDataOptionsDialog : UserControl
    {
        public ReloadXmlDataOptionsDialog()
        {
            InitializeComponent();
        }
    }
}
