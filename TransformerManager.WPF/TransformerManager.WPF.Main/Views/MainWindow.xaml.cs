﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace TransformerManager.WPF.Main.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Rect _rectNormalSize;//定义一个全局rect记录还原状态下窗口的位置和大小。

        public MainWindow()
        {
            InitializeComponent();
        }

        private void OnBtnWindowClosing(object sender, RoutedEventArgs e)
        {
            var result = HandyControl.Controls.MessageBox.Show(
                "是否关闭窗口?",
                "关闭应用?",
                MessageBoxButton.YesNo,
                MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                Environment.Exit(0);
            }

        }

        private void OnBtnWindowMinize(object sender, RoutedEventArgs e)
        {
            SystemCommands.MinimizeWindow(this);
        }

        private void OnBtnWindowMaxize(object sender, RoutedEventArgs e)
        {
            if (this.WindowState == WindowState.Maximized)
            {
                //NormalWindowSize();
                this.WindowState = WindowState.Normal;
            }
            else
            {
                //MaximizeWindowSize();
                this.WindowState = WindowState.Maximized;
            }
        }

        /// <summary>
        /// ScrollViewer的中键滚轮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ScContent_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            var scrollViewer = sender as ScrollViewer;
            var eventArg = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta)
            {
                RoutedEvent = UIElement.MouseWheelEvent,
                Source = sender
            };
            scrollViewer.RaiseEvent(eventArg);
        }

        private void TitleBar_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                if(this.WindowState == WindowState.Maximized)
                {
                    //NormalWindowSize();
                    this.WindowState = WindowState.Normal;
                }
                else
                {
                    //MaximizeWindowSize();
                    this.WindowState = WindowState.Maximized;
                }          
                e.Handled = true;
            }

            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    return;
                }

                this.DragMove();
            }
        }

    }
}
