﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using TransformerManager.EF.Models;

namespace TransformerManager.EF.Database
{
    public class AppDBContext : IdentityDbContext<ApplicationUser>
    {
        public AppDBContext(DbContextOptions options) : base(options)
        {

        }

        /// <summary>
        /// 对应数据库表的映射
        /// </summary>
        public DbSet<Transformer> Transformers { set; get; }
        public DbSet<BasicInfos> BasicInfos { set; get; }
        public DbSet<Tank> Tanks { set; get; }
        public DbSet<Conservator> Conservators { set; get; }
        public DbSet<TapChanger> TapChangers { set; get; }
        public DbSet<CoolingSystem> CoolingSystems { set; get; }
        public DbSet<Dome> Domes { set; get; }
        public DbSet<Bushing> Bushings { set; get; }
        public DbSet<Accessories> Accessories { set; get; }
        public DbSet<AccessoryEquipment> AccessoryEquipments { set; get; }
        public DbSet<Lead> Leads { set; get; }
        public DbSet<LeadConnector> LeadConnectors { set; get; }
        public DbSet<Winding> Windings { set; get; }
        public DbSet<HVRWinding> HVRWindings { set; get; }
        public DbSet<DiscWinding> DiscWindings { set; get; }
        public DbSet<LayerWinding> LayerWindings { set; get; }
        public DbSet<ActivePart> ActiveParts { set; get; }
        public DbSet<RefreshToken> RefreshTokens { set; get; }

        public DbSet<OperationLogs> OperationLogs { set; get; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //var trData = File.ReadAllText(
            //Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)
            //+ @"/Database/Transformer.json");
            //IList<Transformer> transformerInfo = JsonConvert.DeserializeObject<IList<Transformer>>(trData);
            //modelBuilder.Entity<Transformer>().HasData(transformerInfo);

            //var tapChangerData = File.ReadAllText(
            //Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)
            //+ @"/Database/TapChanger.json");
            //IList<TapChanger> tapChangerDataInfo = JsonConvert.DeserializeObject<IList<TapChanger>>(tapChangerData);
            //modelBuilder.Entity<TapChanger>().HasData(tapChangerDataInfo);

            //var tankData = File.ReadAllText(
            //Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)
            //+ @"/Database/Tank.json");
            //IList<Tank> tankDataInfo = JsonConvert.DeserializeObject<IList<Tank>>(tankData);
            //modelBuilder.Entity<Tank>().HasData(tankDataInfo);

            //var domesData = File.ReadAllText(
            //Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)
            //+ @"/Database/Domes.json");
            //IList<Dome> domesDataInfo = JsonConvert.DeserializeObject<IList<Dome>>(domesData);
            //modelBuilder.Entity<Dome>().HasData(domesDataInfo);

            //var coolEquCompData = File.ReadAllText(
            //Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)
            //+ @"/Database/CoolEquComp.json");
            //IList<CoolEquComp> coolEquCompDataInfo = JsonConvert.DeserializeObject<IList<CoolEquComp>>(coolEquCompData);
            //modelBuilder.Entity<CoolEquComp>().HasData(coolEquCompDataInfo);

            //var conservatorData = File.ReadAllText(
            //Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)
            //+ @"/Database/Conservator.json");
            //IList<Conservator> conservatorDataInfo = JsonConvert.DeserializeObject<IList<Conservator>>(conservatorData);
            //modelBuilder.Entity<Conservator>().HasData(conservatorDataInfo);

            //var bushingsData = File.ReadAllText(
            //Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)
            //+ @"/Database/Bushings.json");
            //IList<Bushing> bushingsDataInfo = JsonConvert.DeserializeObject<IList<Bushing>>(bushingsData);
            //modelBuilder.Entity<Bushing>().HasData(bushingsDataInfo);

            // 初始化用户与角色的种子数据
            // 1.更新用户角色的外键
            modelBuilder.Entity<ApplicationUser>(u =>
            u.HasMany(x => x.UserRoles)
            .WithOne().HasForeignKey(ur => ur.UserId).IsRequired()
            );

            // 2.添加管理员角色
            var adminRoleId = "70869555-b878-4ba7-9a16-4403ca155800";
            var superAdminRoleId = "70869555-b878-4ba7-9a16-4403ca155801";
            modelBuilder.Entity<IdentityRole>().HasData(
                new IdentityRole()
                {
                    Id = adminRoleId,
                    Name = "Admin",
                    NormalizedName = "Admin".ToUpper()
                },
                new IdentityRole()
                {
                    Id = superAdminRoleId,
                    Name = "SuperAdmin",
                    NormalizedName = "SuperAdmin".ToUpper()
                }
            );

            // 3.添加用户
            var adminUserId = "70809555-b878-4bc7-9a16-4403ca155912";
            ApplicationUser adminUser = new ApplicationUser()
            {
                Id = adminUserId,
                UserName = "admin",
                NormalizedUserName = "admin",
                Email = "",
                NormalizedEmail = "",
                TwoFactorEnabled = false,
                EmailConfirmed = true,
                PhoneNumber = "",
                PhoneNumberConfirmed = false
            };
            var ph = new PasswordHasher<ApplicationUser>();
            adminUser.PasswordHash = ph.HashPassword(adminUser, "stwhtmd@2021");
            modelBuilder.Entity<ApplicationUser>().HasData(adminUser);

            // 4.给用户加入管理员角色
            modelBuilder.Entity<IdentityUserRole<string>>().HasData(
                new IdentityUserRole<string>()
                {
                    RoleId = adminRoleId,
                    UserId = adminUserId
                },
                new IdentityUserRole<string>()
                {
                    RoleId = superAdminRoleId,
                    UserId = adminUserId
                }
            );

            base.OnModelCreating(modelBuilder);
        }
    }
}
