﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TransformerManager.EF.Models.Enum.TapChanger;

namespace TransformerManager.EF.Models
{
    public class TapChanger
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("BasicInfosId")]
        public int BasicInfosId { set; get; }

        public string ProdType { set; get; }

        public string TapChangerPosition { set; get; }

        public string OperationType { set; get; }

        public string Manufacturer { set; get; }
    }
}
