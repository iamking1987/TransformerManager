﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TransformerManager.EF.Models
{
    public class Winding
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("TransformerId")]
        public Guid TransformerId { set; get; }

        // 调压线圈
        public ICollection<HVRWinding> HVRWindings { set; get; }

        // 饼式线圈
        public ICollection<DiscWinding> DiscWindings { set; get; }

        // 层式线圈
        public ICollection<LayerWinding> LayerWindings { set; get; }
    }
}
