﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TransformerManager.EF.Models.Enum.Lead;

namespace TransformerManager.EF.Models
{
    public class Lead
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("TransformerId")]
        public Guid TransformerId { set; get; }

        // 高压电流
        public double? HVCurrent { set; get; }

        // 低压电流
        public double? LVCurrent { set; get; }

        // 中压电流
        public double? MVCurrent { set; get; }

        // 是否中部出线
        public bool IsCenterInfeed { set; get; }

        // 高压出头方式
        public string HVOutlet { set; get; }

        // 调压出头方式
        public string HVROutlet { set; get; }

        // 低压出头方式
        public string LVOutlet { set; get; }

        // 中压出头方式
        public string MVOutlet { set; get; }

        // 高压套管位置
        public string HVBushingPos { set; get; }

        // 低压套管位置
        public string LVBushingPos { set; get; }

        // 中压套管位置
        public string MVBushingPos { set; get; }

        // 引线连接件
        public ICollection<LeadConnector> LeadConnectors { set; get; }

    }
}
