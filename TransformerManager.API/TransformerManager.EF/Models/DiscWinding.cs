﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TransformerManager.EF.Models.Enum.Winding;

namespace TransformerManager.EF.Models
{
    public class DiscWinding
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("WindingId")]
        public int WindingId { set; get; }

        // 线圈名称
        public string WindingName { set; get; }

        // 饼式线圈类别
        public string DiscWindingType { set; get; }

        // 绕向
        public string WindingDirection { set; get; }

        // 撑条数
        public int StripNumber { set; get; }

        // 垫块数
        public int SpacerNumber { set; get; }

        // 垫块宽度
        public double SpacerWidth { set; get; }

        // 并绕根数
        public int ConductorNumber { set; get; }

        // 是否有静电环
        public bool HasFSA { set; get; }

        // 并绕根数
        public int OilDeflectorNumber { set; get; }

        // 是否是全绝缘
        public bool IsUniformInsulation { set; get; }

        // 是否是CTC导线
        public bool IsCTC { set; get; }

        // 是否有羊角垫块
        public bool HasBananaSpacer { set; get; }

        // 是否有第二饼出线
        public bool HasSecondDiscOut { set; get; }

        // 是否是插花纠
        public bool IsSandwichInterleaved { set; get; }

        // 是否是普纠
        public bool IsInterleaved { set; get; }

        // 导线芯数
        public int ConductorStrandNumber { set; get; }

        // 是否分为两部分
        public bool IsDivided { set; get; }

        // 是否有引线板
        public bool HasLeadingPlate { set; get; }
    }
}
