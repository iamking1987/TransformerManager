﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TransformerManager.EF.Models
{
    public class BasicInfos
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("TransformerId")]
        public Guid TransformerId { set; get; }

        // 项目号
        [Required]
        [MaxLength(6)]
        public string ProjectNumber { set; get; }

        // 项目名
        [Required]
        [MaxLength(150)]
        public string ProjectName { set; get; }

        // 项目状态
        [Required]
        public string ProjectStatus { set; get; }

        // 参考项目号
        public string RefProjectNumber { set; get; }

        // 变压器应用类别
        [Required]
        public string ApplicationType { set; get; }

        // 台数
        [Required]
        public int NumOfUnits { set; get; }

        // 容量
        [Required]
        public double Capacity { set; get; }

        // 工频
        [Required]
        public int RatedFrequency { set; get; }

        // 连接组别
        [Required]
        public string VectorGroupSym { set; get; }

        [Required]
        public double RatedHV { set; get; }

        [Required]
        public double RatedLV { set; get; }

        // 制造日期
        public DateTime ManufactureDate { set; get; }

        // 变压器型号代代号
        public string MaterialNumber { set; get; }

        // 相数
        [Required]
        public int NumberOfPhases { set; get; }

        // 区域划分
        [Required]
        public string RegionType { set; get; }

        // 产品标签
        public string ProdTag { set; get; }

        // 防护方式
        [Required]
        public string Housing { set; get; }

        // 绝缘油
        [Required]
        public string InsulationMaterial { set; get; }

        // 总重
        public double? TotalMass { set; get; }

        public double? Length { set; get; }

        public double? Width { set; get; }

        public double? Height { set; get; }

        public ICollection<TapChanger> TapChangers { set; get; }

        public ICollection<Dome> Domes { set; get; }

        public ICollection<Bushing> Bushings { set; get; }
    }
}
