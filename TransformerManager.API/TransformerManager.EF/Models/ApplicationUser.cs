﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using TransformerManager.EF.Models.Enum;

namespace TransformerManager.EF.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string RealNameEn { set; get; }

        public string RealNameCn { set; get; }

        public ICollection<Transformer> Transformers { set; get; }

        public string Department { set; get; }

        public bool IsLeadEngineer { set; get; }

        public virtual ICollection<IdentityUserRole<string>> UserRoles { set; get; }
        public virtual ICollection<IdentityUserClaim<string>> Claims { set; get; }
        public virtual ICollection<IdentityUserLogin<string>> Logins { set; get; }
        public virtual ICollection<IdentityUserToken<string>> Tokens { set; get; }
    }
}
