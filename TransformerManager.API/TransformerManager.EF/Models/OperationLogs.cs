﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace TransformerManager.EF.Models
{
    public class OperationLogs
    {
        public int Id { get; set; }

        public string Message { get; set; }

        public string MessageTemplate { get; set; }

        public string Level { get; set; }

        public DateTime TimeStamp { get; set; }

        public string Exception { get; set; }

        public string Properties { get; set; }

        public string LogEvent { get; set; }

        public string OperationType { get; set; }

        public string UserName { get; set; }

        public string IP { set; get; }

        public string RequestPath { set; get; }

        public string RequestBody { set; get; }
    }
}
