﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TransformerManager.EF.Models.Enum.Conservator;

namespace TransformerManager.EF.Models
{
    public class Conservator
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("TransformerId")]
        public Guid TransformerId { set; get; }

        // 储油柜类型
        public string ConservatorType { set; get; }

        // 是否有附储油柜
        public bool HasTCConservator { set; get; }

        // 尺寸
        public string Dimension { set; get; }

    }
}
