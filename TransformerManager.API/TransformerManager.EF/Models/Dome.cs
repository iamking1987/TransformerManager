﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TransformerManager.EF.Models.Enum.Dome;

namespace TransformerManager.EF.Models
{
    public class Dome
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("BasicInfosId")]
        public int BasicInfosId { set; get; }

        public string DomeAssembleType { set; get; }

        public double? InnerDiameter { set; get; }

    }
}
