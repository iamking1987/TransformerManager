﻿using System.ComponentModel;

namespace TransformerManager.EF.Models.Enum.Accessories
{
    public enum BoltMaterial
    {
        [Description("A2")]
        A2,

        [Description("A4")]
        A4,

        [Description("Special")]
        Special
    }
}
