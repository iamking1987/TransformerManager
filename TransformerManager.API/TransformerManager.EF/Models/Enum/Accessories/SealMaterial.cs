﻿using System.ComponentModel;

namespace TransformerManager.EF.Models.Enum.Accessories
{
    public enum SealMaterial
    {

        [Description("NBR")]
        NBR,

        [Description("FPM")]
        FPM,

        [Description("ACM")]
        ACM,

        [Description("Others")]
        Others

    }
}
