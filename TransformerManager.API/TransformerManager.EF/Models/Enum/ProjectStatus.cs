﻿using System.ComponentModel;

namespace TransformerManager.EF.Models.Enum
{
    public enum ProjectStatus
    {
        [Description("NotStarted")]
        NotStarted,

        [Description("InProgress")]
        InProgress,

        [Description("Completed")]
        Completed,

        [Description("Deferred")]
        Deferred,

        [Description("Planned")]
        Planned,

        [Description("Waiting")]
        Waiting,

        [Description("Revise")]
        Revise
    }
}
