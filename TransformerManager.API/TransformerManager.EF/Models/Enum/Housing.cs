﻿using System.ComponentModel;

namespace TransformerManager.EF.Models.Enum
{
    public enum Housing
    {
        [Description("NoiseProtectionCover")]
        NoiseProtectionCover,

        [Description("BulletProof")]
        BulletProof,

        [Description("Indoor")]
        Indoor,

        [Description("Outdoor")]
        Outdoor
    }
}
