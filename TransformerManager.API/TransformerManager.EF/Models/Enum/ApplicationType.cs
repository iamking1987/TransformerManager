﻿using System.ComponentModel;

namespace TransformerManager.EF.Models.Enum
{
    public enum ApplicationType
    {
        [Description("None")]
        None,

        [Description("启动变")]
        StartupTransformer, // 启动变

        [Description("接地变")]
        EarthingTransformer, // 接地变

        [Description("自耦变")]
        AutoTransformer, // 自耦变

        [Description("换流变")]
        ConverterTransformer, // 换流变

        [Description("发电升压变")]
        GSUTransformer, // 发电升压变

        [Description("树脂浇注变")]
        CastResignTransformer, // 树脂浇注变

        [Description("电炉变")]
        ElecArcFurnaceTransformer, // 电炉变

        [Description("输电变")]
        NetworkTransformer, // 输电变

        [Description("联络变")]
        SysInterconnTransformer, // 联络变

        [Description("辅助变")]
        AuxiliaryTransformer, // 辅助变

        [Description("整流变")]
        RectifierTransformer, // 整流变

        [Description("线馈变")]
        LineTeederTransformer, // 线馈变

        [Description("移相变")]
        PhaseShifter, // 移相变

        [Description("牵引变")]
        TractionTransformer, // 牵引变

        [Description("油浸配变")]
        LiquidDistributionTransformer, // 油浸配变

        [Description("稳压变")]
        VoltageRegulator, // 稳压变

        [Description("高压直流变")]
        HVDCTransformer //高压直流变
    }
}
