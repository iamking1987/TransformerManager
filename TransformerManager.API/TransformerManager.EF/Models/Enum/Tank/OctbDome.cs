﻿using System.ComponentModel;

namespace TransformerManager.EF.Models.Enum.Tank
{
    public enum OctbDome
    {
        [Description("None")]
        None,

        [Description("SideWall1")]
        SW1,

        [Description("SideWall2")]
        SW2,

        [Description("SideWall3")]
        SW3,

        [Description("SideWall4")]
        SW4,

        [Description("SideWall13")]
        SW13,

        [Description("Others")]
        Others
    }
}
