﻿using System.ComponentModel;

namespace TransformerManager.EF.Models.Enum.Tank
{
    public enum TankShape
    {
        [Description("Quadrilateral")]
        Quadrilateral, // 四角

        [Description("Hexagonal")]
        Hexagonal, // 六角

        [Description("Octagonal")]
        Octagonal,// 八角

        [Description("Polygonal")]
        Polygonal, // 多边形

        [Description("Others")]
        Others // 其他
    }
}
