﻿using System.ComponentModel;

namespace TransformerManager.EF.Models.Enum.Tank
{
    public enum HVLVIndent
    {
        [Description("None")]
        None,

        [Description("SideWall13")]
        SW13
    }
}
