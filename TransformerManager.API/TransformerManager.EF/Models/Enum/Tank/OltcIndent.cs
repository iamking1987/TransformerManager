﻿using System.ComponentModel;

namespace TransformerManager.EF.Models.Enum.Tank
{
    public enum OltcIndent
    {
        [Description("None")]
        None,

        [Description("SideWall2")]
        SW2,

        [Description("SideWall4")]
        SW4,

        [Description("SideWall24")]
        SW24
    }
}
