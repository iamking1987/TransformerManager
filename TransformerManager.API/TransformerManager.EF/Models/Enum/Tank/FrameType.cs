﻿using System.ComponentModel;

namespace TransformerManager.EF.Models.Enum.Tank
{
    public enum FrameType
    {

        [Description("Bolted")]
        Bolted,

        [Description("Welded")]
        Welded
    }
}
