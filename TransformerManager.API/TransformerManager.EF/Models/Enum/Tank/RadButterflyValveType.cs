﻿using System.ComponentModel;

namespace TransformerManager.EF.Models.Enum.Tank
{
    public enum RadButterflyValveType
    {
        [Description("Bolted")]
        Bolted,

        [Description("Welded")]
        Welded,

        [Description("Others")]
        Others
    }
}
