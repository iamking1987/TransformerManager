﻿using System.ComponentModel;

namespace TransformerManager.EF.Models.Enum
{
    public enum Department
    {
        [Description("技术部外部设计")]
        MD_External,

        [Description("技术部内部设计")]
        MD_Internal,

        [Description("技术部电算")]
        ED,

        [Description("其他部门")]
        Others
    }
}
