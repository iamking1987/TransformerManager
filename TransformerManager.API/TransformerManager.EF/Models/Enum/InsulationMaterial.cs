﻿using System.ComponentModel;

namespace TransformerManager.EF.Models.Enum
{
    public enum InsulationMaterial
    {
        [Description("NaturalEsterFluid")]
        NaturalEsterFluid,

        [Description("MineralOil")]
        MineralOil,

        [Description("SiliconeOil")]
        SiliconeOil,

        [Description("SytheticEsterFluid")]
        SytheticEsterFluid,

        [Description("Aramide")]
        Aramide,

        [Description("CastResign")]
        CastResign,

        [Description("EnamelInsulated")]
        EnamelInsulated,

        [Description("Others")]
        Others
    }
}