﻿using System.ComponentModel;

namespace TransformerManager.EF.Models.Enum.Dome
{
    public enum DomeAssembleType
    {
        [Description("Inversion")]
        Inversion,

        [Description("Positive")]
        Positive
    }
}
