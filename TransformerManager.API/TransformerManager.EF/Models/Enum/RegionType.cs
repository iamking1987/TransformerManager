﻿using System.ComponentModel;

namespace TransformerManager.EF.Models.Enum
{
    public enum RegionType
    {
        [Description("Domestic")]
        Domestic,

        [Description("Export")]
        Export
    }
}
