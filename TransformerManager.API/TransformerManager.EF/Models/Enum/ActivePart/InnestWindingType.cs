﻿using System.ComponentModel;

namespace TransformerManager.EF.Models.Enum.ActivePart
{
    public enum InnestWindingType
    {
        [Description("单层")]
        SingleLayer,

        [Description("双层")]
        DoubleLayer,

        [Description("饼式")]
        Disc
    }
}
