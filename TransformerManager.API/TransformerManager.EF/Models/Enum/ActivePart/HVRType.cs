﻿using System.ComponentModel;

namespace TransformerManager.EF.Models.Enum.ActivePart
{
    public enum HVRType
    {
        [Description("调压内置")]
        InsidedSpiral,

        [Description("粗细调")]
        CoarseFine,

        [Description("调压稳压")]
        HVRTV,

        [Description("大螺旋")]
        BigSpiral,

        [Description("调压饼式")]
        DiscHVR
    }
}
