﻿using System.ComponentModel;

namespace TransformerManager.EF.Models.Enum.LeadConnector
{
    public enum LeadName
    {
        [Description("HV")]
        HV,

        [Description("HVR")]
        HVR,

        [Description("LV")]
        LV,

        [Description("MV")]
        MV,

        [Description("TV")]
        TV
    }
}
