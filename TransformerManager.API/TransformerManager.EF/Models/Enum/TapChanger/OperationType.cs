﻿using System.ComponentModel;

namespace TransformerManager.EF.Models.Enum.TapChanger
{
    public enum OperationType
    {

        [Description("OLTC")]
        OLTC,

        [Description("NLTC")]
        NLTC,

        [Description("Others")]
        Others,
    }
}
