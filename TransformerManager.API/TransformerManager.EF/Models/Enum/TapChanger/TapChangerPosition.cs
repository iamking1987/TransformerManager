﻿using System.ComponentModel;

namespace TransformerManager.EF.Models.Enum.TapChanger
{
    public enum TapChangerPosition
    {
        [Description("左侧")]
        LeftSide,

        [Description("右侧")]
        RightSide,

        [Description("高压侧")]
        HVSide,

        [Description("低压侧")]
        LVSide,

        [Description("Others")]
        Others
    }
}
