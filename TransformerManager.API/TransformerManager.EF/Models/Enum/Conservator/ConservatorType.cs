﻿using System.ComponentModel;

namespace TransformerManager.EF.Models.Enum.Conservator
{
    public enum ConservatorType
    {
        [Description("Capsule")]
        Capsule,

        [Description("Corrugate")]
        Corrugate,

        [Description("Others")]
        Others
    }
}
