﻿using System.ComponentModel;

namespace TransformerManager.EF.Models.Enum.Winding
{
    public enum WindingDirection
    {
        [Description("左")]
        Left,

        [Description("右")]
        Right,

        [Description("上左下右")]
        TopLBotR,

        [Description("上右下左")]
        TopRBotL
    }
}
