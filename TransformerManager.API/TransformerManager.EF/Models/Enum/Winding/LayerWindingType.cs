﻿using System.ComponentModel;

namespace TransformerManager.EF.Models.Enum.Winding
{
    public enum LayerWindingType
    {
        [Description("单层")]
        Single,

        [Description("双层")]
        Double,

        [Description("多层")]
        Mutiple
    }
}
