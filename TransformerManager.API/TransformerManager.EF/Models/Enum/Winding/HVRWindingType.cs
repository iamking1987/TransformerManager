﻿using System.ComponentModel;

namespace TransformerManager.EF.Models.Enum.Winding
{
    public enum HVRWindingType
    {
        [Description("27号线圈")]
        W27,

        [Description("31号线圈")]
        W31,

        [Description("大螺旋线圈(上甩头)")]
        BigSpialRight,

        [Description("大螺旋线圈(下甩头)")]
        BigSpialLeft,

        [Description("大螺旋内置线圈")]
        InsidedSpiral,

        [Description("粗细调")]
        CoarseFine
    }
}
