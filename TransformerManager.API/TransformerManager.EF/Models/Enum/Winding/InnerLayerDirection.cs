﻿using System.ComponentModel;

namespace TransformerManager.EF.Models.Enum.Winding
{
    public enum InnerLayerDirection
    {
        [Description("左")]
        Left,

        [Description("右")]
        Right,

        [Description("内左外右")]
        InLOutR,

        [Description("内右外左")]
        InROutL,
    }
}
