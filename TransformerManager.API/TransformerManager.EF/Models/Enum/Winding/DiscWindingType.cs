﻿using System.ComponentModel;

namespace TransformerManager.EF.Models.Enum.Winding
{
    public enum DiscWindingType
    {
        [Description("饼式线圈")]
        Disc,

        [Description("Smith线圈")]
        Smith,

        [Description("调压纠结饼式")]
        Interleaved,

        [Description("调压连续饼式")]
        Continue,

        [Description("调压稳压线圈")]
        HVRTV
    }
}
