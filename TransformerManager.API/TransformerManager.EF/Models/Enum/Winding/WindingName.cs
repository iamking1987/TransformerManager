﻿using System.ComponentModel;

namespace TransformerManager.EF.Models.Enum.Winding
{
    public enum WindingName
    {
        [Description("HV")]
        HV,

        [Description("HVR")]
        HVR,

        [Description("LV")]
        LV,

        [Description("MV")]
        MV,

        [Description("TV")]
        TV
    }
}
