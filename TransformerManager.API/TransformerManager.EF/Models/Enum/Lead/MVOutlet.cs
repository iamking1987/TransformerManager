﻿using System.ComponentModel;

namespace TransformerManager.EF.Models.Enum.Lead
{
    public enum MVOutlet
    {
        [Description("上下轴向")]
        TopBotAxial,

        [Description("上下幅向")]
        TopBotRadial,

        [Description("上中下")]
        TopMidBot
    }
}
