﻿using System.ComponentModel;

namespace TransformerManager.EF.Models.Enum.Lead
{
    public enum HVROutlet
    {

        [Description("#27上右下左")]
        W27TopRightBotLeft,

        [Description("#27上左下右")]
        W27TopLeftBotRight,

        [Description("#31")]
        W31,

        [Description("饼式纠结无出头")]
        NoneOutletDiskTangle,

        [Description("饼式纠结有出头")]
        OutletDiskTangle,

        [Description("大螺旋左")]
        Spiral,

        [Description("大螺旋右")]
        SpiralReverse,

        [Description("特殊")]
        Special
    }
}
