﻿using System.ComponentModel;

namespace TransformerManager.EF.Models.Enum.Lead
{
    public enum HVOutlet
    {
        [Description("上下轴向")]
        TopBotAxial,

        [Description("上下幅向")]
        TopBotRadial,

        [Description("上轴下幅")]
        TopAxialBotRadial,

        [Description("上下辐向中部轴向")]
        TopBotRadialMidRadial
    }
}
