﻿using System.ComponentModel;

namespace TransformerManager.EF.Models.Enum.Lead
{
    public enum BushingPosition
    {
        [Description("左左左")]
        LeftLeftLeft,

        [Description("左中右")]
        LeftMidRight,

        [Description("左右右")]
        LeftRightRight,

        [Description("右右右")]
        RightRightRight,

        [Description("侧出")]
        SideOut,

        [Description("长轴左侧")]
        LongAxisLeftSide,

        [Description("长轴右侧")]
        LongAxisRightSide
    }
}
