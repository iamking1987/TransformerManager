﻿using System.ComponentModel;

namespace TransformerManager.EF.Models.Enum.Lead
{
    public enum LVOutlet
    {

        [Description("上部出")]
        Top,

        [Description("上下出")]
        TopBot,

        [Description("上中下")]
        TopMidBot
    }
}
