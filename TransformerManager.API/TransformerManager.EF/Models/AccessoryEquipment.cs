﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TransformerManager.EF.Models
{
    public class AccessoryEquipment
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("AccessoriesId")]
        public int AccessoriesId { get; set; }

        // 附件装备名称
        public string AccessoryEquName { set; get; }

        // 产品型号
        public string ProdType { set; get; }

        // 制造商
        public string Manufacturer { set; get; }
    }
}
