﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TransformerManager.EF.Models
{
    // 冷却系统
    public class CoolingSystem
    {

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("TransformerId")]
        public Guid TransformerId { set; get; }

        // 冷却方式
        public string CoolingType { set; get; }

        // 布置方式
        public string CoolingLayout { set; get; }

        // 片散型号
        public string RadiatorType { set; get; }

        // 片散制造商
        public string RadiatorManufacturer { set; get; }

        // 风机型号
        public string FanType { set; get; }

        // 风机制造商
        public string FanManufacturer { set; get; }

        // 冷却器型号
        public string CoolerType { set; get; }

        // 冷却器制造商
        public string CoolerManufacturer { set; get; }
    }
}
