﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TransformerManager.EF.Models.Enum.Accessories;

namespace TransformerManager.EF.Models
{
    public class Accessories
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("TransformerId")]
        public Guid TransformerId { set; get; }

        // 螺栓材质
        public string BoltMaterial { set; get; }

        // 螺栓材质
        public string SealMaterial { set; get; }

        // 附件装备
        public ICollection<AccessoryEquipment> AccesstoryEquipments { set; get; }
    }
}
