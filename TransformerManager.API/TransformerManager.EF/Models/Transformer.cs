﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TransformerManager.EF.Models
{
    public class Transformer
    {
        [Key]
        public Guid Id { set; get; }

        [ForeignKey("UserId")]
        public string UserId { set; get; }

        public BasicInfos BasicInfos { set; get; }

        public Tank Tank { set; get; }

        public Conservator Conservator { set; get; }

        public CoolingSystem CoolingSystem { set; get; }

        public Accessories Accessories { set; get; }

        public Lead Lead { set; get; }

        public Winding Winding { set; get; }

        public ActivePart ActivePart { set; get; }

        public DateTime CreateTime { get; set; }

        public DateTime? UpdateTime { get; set; }
    }
}
