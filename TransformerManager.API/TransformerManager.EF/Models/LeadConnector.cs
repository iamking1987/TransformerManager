﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TransformerManager.EF.Models.Enum.LeadConnector;

namespace TransformerManager.EF.Models
{
    public class LeadConnector
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("LeadId")]
        public int LeadId { set; get; }

        // 引线名称
        public string LeadName { set; get; }

        // 引线类别
        public string LeadType { set; get; }

        // 引线尺寸类别
        public string LeadDimensionType { set; get; }

        // 绝缘厚度
        public double? Si { set; get; }
    }
}
