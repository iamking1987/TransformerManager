﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TransformerManager.EF.Migrations
{
    public partial class InitDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    RealNameEn = table.Column<string>(nullable: true),
                    RealNameCn = table.Column<string>(nullable: true),
                    Department = table.Column<string>(nullable: true),
                    IsLeadEngineer = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OperationLogs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Message = table.Column<string>(nullable: true),
                    MessageTemplate = table.Column<string>(nullable: true),
                    Level = table.Column<string>(nullable: true),
                    TimeStamp = table.Column<DateTime>(nullable: false),
                    Exception = table.Column<string>(nullable: true),
                    Properties = table.Column<string>(nullable: true),
                    LogEvent = table.Column<string>(nullable: true),
                    OperationType = table.Column<string>(nullable: true),
                    UserName = table.Column<string>(nullable: true),
                    IP = table.Column<string>(nullable: true),
                    RequestPath = table.Column<string>(nullable: true),
                    RequestBody = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OperationLogs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    ApplicationUserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_ApplicationUserId",
                        column: x => x.ApplicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false),
                    ApplicationUserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_ApplicationUserId",
                        column: x => x.ApplicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false),
                    ApplicationUserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_ApplicationUserId",
                        column: x => x.ApplicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true),
                    ApplicationUserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_ApplicationUserId",
                        column: x => x.ApplicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RefreshTokens",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    JwtId = table.Column<string>(maxLength: 128, nullable: false),
                    Token = table.Column<string>(maxLength: 256, nullable: false),
                    IsUsed = table.Column<bool>(nullable: false),
                    Invalidated = table.Column<bool>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    ExpiryTime = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RefreshTokens", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RefreshTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Transformers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserId = table.Column<string>(nullable: true),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    UpdateTime = table.Column<DateTime>(nullable: true),
                    ApplicationUserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transformers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Transformers_AspNetUsers_ApplicationUserId",
                        column: x => x.ApplicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Accessories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TransformerId = table.Column<Guid>(nullable: false),
                    BoltMaterial = table.Column<string>(nullable: true),
                    SealMaterial = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accessories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Accessories_Transformers_TransformerId",
                        column: x => x.TransformerId,
                        principalTable: "Transformers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ActiveParts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TransformerId = table.Column<Guid>(nullable: false),
                    StripNumber = table.Column<int>(nullable: false),
                    SpacerNumber = table.Column<int>(nullable: false),
                    RatedVotage = table.Column<int>(nullable: false),
                    IsCenterInfeed = table.Column<bool>(nullable: false),
                    IsUniformInsulation = table.Column<bool>(nullable: false),
                    WindingNumber = table.Column<int>(nullable: false),
                    HVRType = table.Column<string>(nullable: true),
                    InnestWindingType = table.Column<string>(nullable: true),
                    HVAC = table.Column<int>(nullable: false),
                    HasOD = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActiveParts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ActiveParts_Transformers_TransformerId",
                        column: x => x.TransformerId,
                        principalTable: "Transformers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BasicInfos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TransformerId = table.Column<Guid>(nullable: false),
                    ProjectNumber = table.Column<string>(maxLength: 6, nullable: false),
                    ProjectName = table.Column<string>(maxLength: 150, nullable: false),
                    ProjectStatus = table.Column<string>(nullable: false),
                    RefProjectNumber = table.Column<string>(nullable: true),
                    ApplicationType = table.Column<string>(nullable: false),
                    NumOfUnits = table.Column<int>(nullable: false),
                    Capacity = table.Column<double>(nullable: false),
                    RatedFrequency = table.Column<int>(nullable: false),
                    VectorGroupSym = table.Column<string>(nullable: false),
                    RatedHV = table.Column<double>(nullable: false),
                    RatedLV = table.Column<double>(nullable: false),
                    ManufactureDate = table.Column<DateTime>(nullable: false),
                    MaterialNumber = table.Column<string>(nullable: true),
                    NumberOfPhases = table.Column<int>(nullable: false),
                    RegionType = table.Column<string>(nullable: false),
                    ProdTag = table.Column<string>(nullable: true),
                    Housing = table.Column<string>(nullable: false),
                    InsulationMaterial = table.Column<string>(nullable: false),
                    TotalMass = table.Column<double>(nullable: true),
                    Length = table.Column<double>(nullable: true),
                    Width = table.Column<double>(nullable: true),
                    Height = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BasicInfos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BasicInfos_Transformers_TransformerId",
                        column: x => x.TransformerId,
                        principalTable: "Transformers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Conservators",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TransformerId = table.Column<Guid>(nullable: false),
                    ConservatorType = table.Column<string>(nullable: true),
                    HasTCConservator = table.Column<bool>(nullable: false),
                    Dimension = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Conservators", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Conservators_Transformers_TransformerId",
                        column: x => x.TransformerId,
                        principalTable: "Transformers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CoolingSystems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TransformerId = table.Column<Guid>(nullable: false),
                    CoolingType = table.Column<string>(nullable: true),
                    CoolingLayout = table.Column<string>(nullable: true),
                    RadiatorType = table.Column<string>(nullable: true),
                    RadiatorManufacturer = table.Column<string>(nullable: true),
                    FanType = table.Column<string>(nullable: true),
                    FanManufacturer = table.Column<string>(nullable: true),
                    CoolerType = table.Column<string>(nullable: true),
                    CoolerManufacturer = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoolingSystems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CoolingSystems_Transformers_TransformerId",
                        column: x => x.TransformerId,
                        principalTable: "Transformers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Leads",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TransformerId = table.Column<Guid>(nullable: false),
                    HVCurrent = table.Column<double>(nullable: true),
                    LVCurrent = table.Column<double>(nullable: true),
                    MVCurrent = table.Column<double>(nullable: true),
                    IsCenterInfeed = table.Column<bool>(nullable: false),
                    HVOutlet = table.Column<string>(nullable: true),
                    HVROutlet = table.Column<string>(nullable: true),
                    LVOutlet = table.Column<string>(nullable: true),
                    MVOutlet = table.Column<string>(nullable: true),
                    HVBushingPos = table.Column<string>(nullable: true),
                    LVBushingPos = table.Column<string>(nullable: true),
                    MVBushingPos = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Leads", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Leads_Transformers_TransformerId",
                        column: x => x.TransformerId,
                        principalTable: "Transformers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Tanks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TransformerId = table.Column<Guid>(nullable: false),
                    TankLength = table.Column<double>(nullable: true),
                    TankWidth = table.Column<double>(nullable: true),
                    TankHeight = table.Column<double>(nullable: true),
                    TankShape = table.Column<string>(nullable: true),
                    FrameType = table.Column<string>(nullable: true),
                    RadButterflyValveType = table.Column<string>(nullable: true),
                    OltcIndent = table.Column<string>(nullable: true),
                    HVLVIndent = table.Column<string>(nullable: true),
                    ActbDome = table.Column<string>(nullable: true),
                    OctbDome = table.Column<string>(nullable: true),
                    HasWheel = table.Column<bool>(nullable: false),
                    HasNeutralDome = table.Column<bool>(nullable: false),
                    HasEnlargeSideWall1 = table.Column<bool>(nullable: false),
                    HasEnlargeSideWall2 = table.Column<bool>(nullable: false),
                    HasEnlargeSideWall3 = table.Column<bool>(nullable: false),
                    HasEnlargeSideWall4 = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tanks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tanks_Transformers_TransformerId",
                        column: x => x.TransformerId,
                        principalTable: "Transformers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Windings",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TransformerId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Windings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Windings_Transformers_TransformerId",
                        column: x => x.TransformerId,
                        principalTable: "Transformers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AccessoryEquipments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AccessoriesId = table.Column<int>(nullable: false),
                    AccessoryEquName = table.Column<string>(nullable: true),
                    ProdType = table.Column<string>(nullable: true),
                    Manufacturer = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccessoryEquipments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccessoryEquipments_Accessories_AccessoriesId",
                        column: x => x.AccessoriesId,
                        principalTable: "Accessories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Bushings",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BasicInfosId = table.Column<int>(nullable: false),
                    InsulatorColor = table.Column<string>(nullable: true),
                    CTTotalAssembleHeight = table.Column<double>(nullable: true),
                    ProdType = table.Column<string>(nullable: true),
                    RefStandard = table.Column<string>(nullable: true),
                    Manufacturer = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bushings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Bushings_BasicInfos_BasicInfosId",
                        column: x => x.BasicInfosId,
                        principalTable: "BasicInfos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Domes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BasicInfosId = table.Column<int>(nullable: false),
                    DomeAssembleType = table.Column<string>(nullable: true),
                    InnerDiameter = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Domes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Domes_BasicInfos_BasicInfosId",
                        column: x => x.BasicInfosId,
                        principalTable: "BasicInfos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TapChangers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BasicInfosId = table.Column<int>(nullable: false),
                    ProdType = table.Column<string>(nullable: true),
                    TapChangerPosition = table.Column<string>(nullable: true),
                    OperationType = table.Column<string>(nullable: true),
                    Manufacturer = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TapChangers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TapChangers_BasicInfos_BasicInfosId",
                        column: x => x.BasicInfosId,
                        principalTable: "BasicInfos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LeadConnectors",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LeadId = table.Column<int>(nullable: false),
                    LeadName = table.Column<string>(nullable: true),
                    LeadType = table.Column<string>(nullable: true),
                    LeadDimensionType = table.Column<string>(nullable: true),
                    Si = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LeadConnectors", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LeadConnectors_Leads_LeadId",
                        column: x => x.LeadId,
                        principalTable: "Leads",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DiscWindings",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    WindingId = table.Column<int>(nullable: false),
                    WindingName = table.Column<string>(nullable: true),
                    DiscWindingType = table.Column<string>(nullable: true),
                    WindingDirection = table.Column<string>(nullable: true),
                    StripNumber = table.Column<int>(nullable: false),
                    SpacerNumber = table.Column<int>(nullable: false),
                    SpacerWidth = table.Column<double>(nullable: false),
                    ConductorNumber = table.Column<int>(nullable: false),
                    HasFSA = table.Column<bool>(nullable: false),
                    OilDeflectorNumber = table.Column<int>(nullable: false),
                    IsUniformInsulation = table.Column<bool>(nullable: false),
                    IsCTC = table.Column<bool>(nullable: false),
                    HasBananaSpacer = table.Column<bool>(nullable: false),
                    HasSecondDiscOut = table.Column<bool>(nullable: false),
                    IsSandwichInterleaved = table.Column<bool>(nullable: false),
                    IsInterleaved = table.Column<bool>(nullable: false),
                    ConductorStrandNumber = table.Column<int>(nullable: false),
                    IsDivided = table.Column<bool>(nullable: false),
                    HasLeadingPlate = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiscWindings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DiscWindings_Windings_WindingId",
                        column: x => x.WindingId,
                        principalTable: "Windings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HVRWindings",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    WindingId = table.Column<int>(nullable: false),
                    HVRWindingType = table.Column<string>(nullable: true),
                    WindingDirection = table.Column<string>(nullable: true),
                    StripNumber = table.Column<int>(nullable: false),
                    SpacerNumber = table.Column<int>(nullable: false),
                    SpacerWidth = table.Column<double>(nullable: true),
                    ConductorNumber = table.Column<int>(nullable: false),
                    HasFSA = table.Column<bool>(nullable: false),
                    IsUniformInsulation = table.Column<bool>(nullable: false),
                    IsCTC = table.Column<bool>(nullable: false),
                    HVRStepNumber = table.Column<int>(nullable: false),
                    TopKsNumber = table.Column<int>(nullable: false),
                    AngleRingNumber = table.Column<int>(nullable: false),
                    IsDivided = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HVRWindings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HVRWindings_Windings_WindingId",
                        column: x => x.WindingId,
                        principalTable: "Windings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LayerWindings",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    WindingId = table.Column<int>(nullable: false),
                    WindingName = table.Column<string>(nullable: true),
                    LayerWindingType = table.Column<string>(nullable: true),
                    InnerLayerDirection = table.Column<string>(nullable: true),
                    StripNumber = table.Column<int>(nullable: false),
                    SpacerNumber = table.Column<int>(nullable: false),
                    SpacerWidth = table.Column<double>(nullable: false),
                    AsialConductorNumber = table.Column<int>(nullable: false),
                    RadialConductorNumber = table.Column<int>(nullable: false),
                    HasFSA = table.Column<bool>(nullable: false),
                    OilDeflectorNumber = table.Column<int>(nullable: false),
                    IsUniformInsulation = table.Column<bool>(nullable: false),
                    InnerCylinderNumber = table.Column<int>(nullable: false),
                    HasCoreShielding = table.Column<bool>(nullable: false),
                    IsLever = table.Column<bool>(nullable: false),
                    IsDivided = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LayerWindings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LayerWindings_Windings_WindingId",
                        column: x => x.WindingId,
                        principalTable: "Windings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "70869555-b878-4ba7-9a16-4403ca155800", "10c4a92e-0736-4d87-9962-eaf8eaba8f4b", "Admin", "ADMIN" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "70869555-b878-4ba7-9a16-4403ca155801", "094e373b-091d-4c6b-851c-2249ea8c59cf", "SuperAdmin", "SUPERADMIN" });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Department", "Email", "EmailConfirmed", "IsLeadEngineer", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "RealNameCn", "RealNameEn", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { "70809555-b878-4bc7-9a16-4403ca155912", 0, "d34bb76c-aa7c-4845-95a1-8195ec43b908", null, "", true, false, false, null, "", "admin", "AQAAAAEAACcQAAAAEPH+LxUz4CIyZT+wzKle/qQpkXKbErvh81ISPU/WyQbkr5caz8WyYaazvcV4qWAt9Q==", "", false, null, null, "87453ca9-e3e0-48ab-9bce-f28767ab0401", false, "admin" });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId", "ApplicationUserId" },
                values: new object[] { "70809555-b878-4bc7-9a16-4403ca155912", "70869555-b878-4ba7-9a16-4403ca155800", null });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId", "ApplicationUserId" },
                values: new object[] { "70809555-b878-4bc7-9a16-4403ca155912", "70869555-b878-4ba7-9a16-4403ca155801", null });

            migrationBuilder.CreateIndex(
                name: "IX_Accessories_TransformerId",
                table: "Accessories",
                column: "TransformerId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AccessoryEquipments_AccessoriesId",
                table: "AccessoryEquipments",
                column: "AccessoriesId");

            migrationBuilder.CreateIndex(
                name: "IX_ActiveParts_TransformerId",
                table: "ActiveParts",
                column: "TransformerId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_ApplicationUserId",
                table: "AspNetUserClaims",
                column: "ApplicationUserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_ApplicationUserId",
                table: "AspNetUserLogins",
                column: "ApplicationUserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_ApplicationUserId",
                table: "AspNetUserRoles",
                column: "ApplicationUserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserTokens_ApplicationUserId",
                table: "AspNetUserTokens",
                column: "ApplicationUserId");

            migrationBuilder.CreateIndex(
                name: "IX_BasicInfos_TransformerId",
                table: "BasicInfos",
                column: "TransformerId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Bushings_BasicInfosId",
                table: "Bushings",
                column: "BasicInfosId");

            migrationBuilder.CreateIndex(
                name: "IX_Conservators_TransformerId",
                table: "Conservators",
                column: "TransformerId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CoolingSystems_TransformerId",
                table: "CoolingSystems",
                column: "TransformerId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DiscWindings_WindingId",
                table: "DiscWindings",
                column: "WindingId");

            migrationBuilder.CreateIndex(
                name: "IX_Domes_BasicInfosId",
                table: "Domes",
                column: "BasicInfosId");

            migrationBuilder.CreateIndex(
                name: "IX_HVRWindings_WindingId",
                table: "HVRWindings",
                column: "WindingId");

            migrationBuilder.CreateIndex(
                name: "IX_LayerWindings_WindingId",
                table: "LayerWindings",
                column: "WindingId");

            migrationBuilder.CreateIndex(
                name: "IX_LeadConnectors_LeadId",
                table: "LeadConnectors",
                column: "LeadId");

            migrationBuilder.CreateIndex(
                name: "IX_Leads_TransformerId",
                table: "Leads",
                column: "TransformerId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_RefreshTokens_UserId",
                table: "RefreshTokens",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Tanks_TransformerId",
                table: "Tanks",
                column: "TransformerId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TapChangers_BasicInfosId",
                table: "TapChangers",
                column: "BasicInfosId");

            migrationBuilder.CreateIndex(
                name: "IX_Transformers_ApplicationUserId",
                table: "Transformers",
                column: "ApplicationUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Windings_TransformerId",
                table: "Windings",
                column: "TransformerId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AccessoryEquipments");

            migrationBuilder.DropTable(
                name: "ActiveParts");

            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "Bushings");

            migrationBuilder.DropTable(
                name: "Conservators");

            migrationBuilder.DropTable(
                name: "CoolingSystems");

            migrationBuilder.DropTable(
                name: "DiscWindings");

            migrationBuilder.DropTable(
                name: "Domes");

            migrationBuilder.DropTable(
                name: "HVRWindings");

            migrationBuilder.DropTable(
                name: "LayerWindings");

            migrationBuilder.DropTable(
                name: "LeadConnectors");

            migrationBuilder.DropTable(
                name: "OperationLogs");

            migrationBuilder.DropTable(
                name: "RefreshTokens");

            migrationBuilder.DropTable(
                name: "Tanks");

            migrationBuilder.DropTable(
                name: "TapChangers");

            migrationBuilder.DropTable(
                name: "Accessories");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "Windings");

            migrationBuilder.DropTable(
                name: "Leads");

            migrationBuilder.DropTable(
                name: "BasicInfos");

            migrationBuilder.DropTable(
                name: "Transformers");

            migrationBuilder.DropTable(
                name: "AspNetUsers");
        }
    }
}
