using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;
using System;
using TransformerManager.API.LogSettings;

namespace WebApplication1
{
    public class Program
    {
        public static void Main(string[] args)
        {
            IConfigurationRoot configuration =
                new ConfigurationBuilder()
                .AddJsonFile(
                    "appsettings.json", 
                    optional: false, 
                    reloadOnChange: true
                    ).Build();

            SerilogConfiguration.CreateLogger();

            try
            {
                Log.Information("Try to run program!");
                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "The program start failed!");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();

                    webBuilder.UseSerilog();
                });
    }
}
