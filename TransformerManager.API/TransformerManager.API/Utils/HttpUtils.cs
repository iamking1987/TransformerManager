﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TransformerManager.Shared.Helper;

namespace TransformerManager.API.Utils
{
    public static class HttpUtils
    {

        /// <summary>
        /// 为Get请求返回的头部数据生成前一页和后一页的URL
        /// </summary>
        /// <param name="paginationParams">分页参数</param>
        /// <param name="routeName">需要生成URL的路由名称</param>
        /// <param name="type">生成URL的类别(枚举,前一页或后一页)</param>
        /// <param name="urlHelper">URLHelper对象</param>
        /// <returns>生成的URL链接</returns>
        public static string GenerateRouteResourceURL(
            PaginationResourceParameters paginationParams,
            string routeName,
            ResourceUrlType type,
            IUrlHelper urlHelper)
        {

            return type switch
            {
                ResourceUrlType.PreviousPage => urlHelper.Link(routeName,
                        new
                        {
                            pageNumber = paginationParams.PageNumber - 1,
                            pageSize = paginationParams.PageSize
                        }),
                ResourceUrlType.NextPage => urlHelper.Link(routeName,
                        new
                        {
                            pageNumber = paginationParams.PageNumber + 1,
                            pageSize = paginationParams.PageSize
                        }),
                _ => throw new NotImplementedException()
            };
        }
    }
}
