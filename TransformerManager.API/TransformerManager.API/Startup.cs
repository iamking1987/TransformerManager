using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Serialization;
using Quartz;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using TransformerManager.EF.Database;
using TransformerManager.API.LogSettings;
using TransformerManager.EF.Models;
using TransformerManager.API.Quartz.Jobs;
using TransformerManager.Shared.Servers;
using AutoMapper;
using System.Reflection;
using TransformerManager.Shared.Helper.HttpParams;

namespace WebApplication1
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // 添加API版本控制
            services.AddApiVersioning(options =>
            {
                options.ReportApiVersions = true;
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.DefaultApiVersion = new ApiVersion(1, 0);
            });

            // 添加自定义用户类
            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<AppDBContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                //是否要求有数字
                options.Password.RequireDigit = true;
                //是否要求有小写的ASCII字母
                options.Password.RequireLowercase = false;
                //是否要求有非字母数字的字符
                options.Password.RequireNonAlphanumeric = false;
                //是否要求有大写的ASCII字母
                options.Password.RequireUppercase = false;
                //密码要求的最小长度
                options.Password.RequiredLength = 6;
                options.Password.RequiredUniqueChars = 1;
            });

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                var secretByte = Encoding.UTF8.GetBytes(Configuration["Authenticatiton:SecretKey"]);
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = true,
                    ValidIssuer = Configuration["Authenticatiton:Issuer"],

                    ValidateAudience = true,
                    ValidAudience = Configuration["Authenticatiton:Audience"],

                    ValidateLifetime = true,

                    // 对密钥进行对称加密
                    IssuerSigningKey = new SymmetricSecurityKey(secretByte)
                };

            });

            // 添加控制器
            services.AddControllers(setupAction =>
            {
                setupAction.ReturnHttpNotAcceptable = true;
                //setupAction.OutputFormatters.Add(
                //    new Microsoft.AspNetCore.Mvc.Formatters.XmlDataContractSerializerOutputFormatter());
            })
            .AddNewtonsoftJson(setupAction =>
            {
                //设置序列化时key为驼峰样式
                setupAction.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            })
            .AddXmlDataContractSerializerFormatters()
            // 添加api行为选项，主要是让程序能返回422错误状态码，仅在controller的验证IsValid为false执行
            .ConfigureApiBehaviorOptions(setupAction =>
            {
                setupAction.InvalidModelStateResponseFactory = context =>
                {
                    var problemDetail = new ValidationProblemDetails(context.ModelState)
                    {
                        Type = "",
                        Title = "传递参数不符合验证规范",
                        Status = StatusCodes.Status422UnprocessableEntity,
                        Detail = "请查看详细说明",
                        Instance = context.HttpContext.Request.Path
                    };
                    problemDetail.Extensions.Add("traceId", context.HttpContext.TraceIdentifier);

                    // 自定义422错误的响应返回类型
                    var apiResponseParams = new ApiResponseParams("传递参数不符合验证规范", problemDetail);

                    return new UnprocessableEntityObjectResult(apiResponseParams)
                    {
                        ContentTypes = { "application/problem+json" }
                    };
                };
            });

            services.AddTransient<ITransformerRepository, TransformerRepository>();

            services.AddTransient<IUserService, UserService>();

            services.AddTransient<IOperationLogService, OperationLogService>();
            // 配置数据库参数
            services.AddDbContext<AppDBContext>(options =>
            {
                options.UseSqlServer(Configuration["DbContext:ConnectionString"]);
                //options.UseMySql(Configuration["DbContext:MySQLConnectionString"]);
            });
            // 自动扫描profile文件完成model和dto映射

            services.AddAutoMapper(Assembly.Load("TransformerManager.Shared"));

            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();

            services.AddSwaggerGen(options =>
            {
                options.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
                options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorizaion header",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Scheme = "bearer",
                    Type = SecuritySchemeType.Http,
                    BearerFormat = "JWT"
                });
                options.AddSecurityRequirement(new OpenApiSecurityRequirement {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new List<string>()
                    }
                });
                options.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Title = "TransformerManager.API 接口文档",
                    Version = "v1",
                    Description = "RESTful API for TransformerManager",
                    TermsOfService = new Uri("https://example.com/terms"),
                    Contact = new OpenApiContact
                    {
                        Name = "Lin Xiao Yi",
                        Email = "xiaoyi.lin@siemens-energy.com"
                    }
                });
                // 为 Swagger JSON and UI设置xml文档注释路径
                var basePath = Path.GetDirectoryName(typeof(Program).Assembly.Location);//获取应用程序所在目录（绝对，不受工作目录影响，建议采用此方法获取路径）
                var xmlPath = Path.Combine(basePath, "TransformerManager.API.xml");
                options.IncludeXmlComments(xmlPath);
            });

            // 设置定时处理事件
            services.AddQuartz(q =>
            {
                q.UseMicrosoftDependencyInjectionJobFactory();

                var jobKey = new JobKey("DeleteOpLogsJob", "OpLogs");

                q.AddJob<DeleteOpLogsJob>(j => j.WithIdentity(jobKey));

                q.AddTrigger(t => t
                   .WithIdentity("Cron Trigger", "OpLogs")
                   .StartNow()
                   .WithCronSchedule(Configuration["Quartz:DelOperationLogsCronSchedule"])
                   .ForJob(jobKey));
            });

            services.AddQuartzServer(q => q.WaitForJobsToComplete = true);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseSerilogRequestLogging();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                app.UseSwagger();

                app.UseSwaggerUI(options =>
                {
                    options.SwaggerEndpoint("/swagger/v1/swagger.json", "TransformerManager.API v1");
                    //options.RoutePrefix = string.Empty;
                });
            }
            
            //app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            // 自定义Serilog的中间件
            app.UseMiddleware<HttpContextLogMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
